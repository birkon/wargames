package edu.ntnu.idatt2001.wargamesdel1.army;

import edu.ntnu.idatt2001.wargamesdel1.units.*;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Army class to store units
 * and army name
 *
 * Used when displaying all units and editing them
 *
 * @author birk
 * @version 23.05.2022
 */
public class Army {


    //name of army
    private String name;

    //units in army
    private final ArrayList<Unit> units;

    /**
     * assignes name, and creates units list
     * @param name name of army type string
     */
    public Army(String name) {
        this.name = name;
        this.units = new ArrayList<>();
    }

    /**
     * Overload in case you want to pass a units list when constructed
     * @param name name of army type string
     */
    public Army(String name, ArrayList<Unit> Units) {
        this.name = name;
        this.units = Units;
    }

    /**
     * copy constructor
     * @param armyToCopy other army to deep copy
     * @throws IllegalArgumentException if armyToCopy is null
     */
    public Army(Army armyToCopy) throws IllegalArgumentException{
        if (armyToCopy == null)
            throw new IllegalArgumentException("army to copy cant be null");
        this.name = armyToCopy.getName();
        this.units = new ArrayList<>(armyToCopy.getAllUnits());
    }

    /**
     * returns the name of the army
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * sets the name
     * @param name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * copys a unit
     * used multiple times in army class to remove references
     * @param unit unit to copy
     * @return copied unit
     */
    private Unit copyUnit(Unit unit){
        if (unit.getClass() == CavalryUnit.class)
            return new CavalryUnit(unit);
        else
        if (unit.getClass() == ComanderUnit.class)
            return new ComanderUnit(unit);
        else
        if (unit.getClass() == InfantryUnit.class)
            return new InfantryUnit(unit);
        else
        if (unit.getClass() == RangedUnit.class)
            return new RangedUnit(unit);
        return null;
    }

    /**
     * adds unit to units list
     * deepcopys of the unit is added
     * @param unit unit to add
     * @throws IllegalArgumentException when unit class is not handeld
     */
    public void addUnit(Unit unit) throws IllegalArgumentException{
        Unit copiedUnit = copyUnit(unit);
        if (copiedUnit != null)
            units.add(copiedUnit);
        else
            throw new IllegalArgumentException("Unit class not handeld in army");
    }

    /**
     * loops a list and calls addUnit method on elements
     * @param units units to add
     * @throws IllegalArgumentException if unit class is not handeld
     */
    public void addAll(List<Unit> units) throws IllegalArgumentException {
        units.forEach(this::addUnit);
    }

    /**
     * removes a unit from units
     * @param unit unit to remove
     */
    public void remove(Unit unit){
        units.remove(unit);
    }

    /**
     * removes a list of units from units
     * @param unitsToRemove unit to remove
     */
    public void remove(List<Unit> unitsToRemove){
        unitsToRemove.forEach(units::remove);
    }


    /**
     * copys all units and ruturns copied list
     * @return arraylsit of all units
     */
    public List<Unit> getAllUnits(){
        return new ArrayList<>(units.stream().map(this::copyUnit).toList());
    }

    /**
     * copys all units that matches and ruturns copied list
     * @return arraylsit of all units that matches UnitTableElement
     */
    public List<Unit> getAllUnitsOfUnitTableElement(UnitTableElement unit){
        return new ArrayList<>(units.stream().map(this::copyUnit).filter(e-> e.getUnitType().equals(unit.getUnitType())
                && e.getHealth() == Integer.parseInt(unit.getUnitHealth())
                && e.getName().equalsIgnoreCase(unit.getUnitName())).toList()
        );
    }

    /**
     * returns a random unit from units
     * this is not a deep copy, becouse we want to save changes to units list
     * @return unit null if no unit is found
     */
    public Unit getRandomUnit(){
        try {
            return units.get((int) (Math.random() * (units.size() -1)));
        }catch (IndexOutOfBoundsException e){
            return null;
        }
    }

    /**
     * returns all infantryUnits
     * @return ArrayList of unit
     */
    public List<Unit> getInfatryUnits(){
        return new ArrayList<>(units.stream().filter(e-> e.getClass() == InfantryUnit.class).toList());
    }

    /**
     * returns all infantryUnits
     * @return ArrayList of unit
     */
    public List<Unit> getCavalryUnits(){
        return new ArrayList<>( units.stream().filter(e-> e.getClass() == CavalryUnit.class).toList());
    }

    /**
     * returns all infantryUnits
     * @return ArrayList of unit
     */
    public List<Unit> getRangedUnit(){
        return new ArrayList<>(units.stream().filter(e-> e.getClass() == RangedUnit.class).toList());
    }

    /**
     * returns all infantryUnits
     * @return ArrayList of unit
     */
    public List<Unit> getComanderUnit(){
        return new ArrayList<>(units.stream().filter(e-> e.getClass() == ComanderUnit.class).toList());
    }

    /**
     * Loops the units arrayList and creates array of unique names
     * @return array of names
     */
    private List<String> getUnitNames(){
        ArrayList<String> unitTypes = new ArrayList<>();
        for (Unit unit : this.units){
            if (!unitTypes.contains(unit.getName()))
                unitTypes.add(unit.getName());
        }

        return unitTypes;
    }


    /**
     * creates unitTableElements from units array
     * sorts them inn to different arrays then creates elements
     * @param useHealth if it shoud also sort by health
     * @return list of unitTableElements
     */
    public List<UnitTableElement> getUnitTableElements(boolean useHealth){
        ArrayList<Unit[]> sortedByName = new ArrayList<>();
        ArrayList<Unit[]> SortedByTypeAndNameAndHealth = new ArrayList<>();


        getUnitNames().forEach(e->
                sortedByName.add(
                units.stream()
                .filter(unit -> unit.getName().equalsIgnoreCase(e))
                        .toArray(Unit[]::new)));

        sortedByName.forEach(e->
            Arrays.stream(e).forEach(x->{
                boolean addNewList = true;
                for(Unit[] unitsSubArray : SortedByTypeAndNameAndHealth){
                    if(unitsSubArray[0].getUnitType().equals(x.getUnitType())
                            && unitsSubArray[0].getHealth() == x.getHealth()
                            && unitsSubArray[0].getName().equalsIgnoreCase(x.getName())){

                        SortedByTypeAndNameAndHealth.remove(unitsSubArray);
                        ArrayList<Unit> tempNewList = new ArrayList<>(Arrays.stream(unitsSubArray).toList());
                        tempNewList.add(x);
                        SortedByTypeAndNameAndHealth.add(tempNewList.toArray(Unit[]::new));
                        addNewList = false;
                        break;
                    }
            }
            if(addNewList)
                SortedByTypeAndNameAndHealth.add(new Unit[]{x});
        }));



        if (useHealth)
            return SortedByTypeAndNameAndHealth.stream().map(e-> new UnitTableElement(e[0].getName(),
                    e.length, String.valueOf(e[0].getHealth()), e[0].getUnitType())).toList();
        else
            return sortedByName.stream().map(e-> new UnitTableElement(e[0].getName(), e.length,
                    "0", e[0].getUnitType())).toList();
    }

    /**
     * lists all units in one string
     * @return string of all units
     */
    @Override
    public String toString() {
        return getName()+": \n"+units.stream().map(Unit::toString).collect(Collectors.joining("\n"));
    }


}
