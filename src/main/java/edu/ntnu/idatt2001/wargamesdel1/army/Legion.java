package edu.ntnu.idatt2001.wargamesdel1.army;

import edu.ntnu.idatt2001.wargamesdel1.game.GlobalData;
import edu.ntnu.idatt2001.wargamesdel1.sim.Fight;
import edu.ntnu.idatt2001.wargamesdel1.units.*;

import java.util.ArrayList;
import java.util.List;


/**
 * A block of units
 * used to connect the movment of mulitple units
 */
public class Legion {
    private final ArrayList<Unit> units = new ArrayList<>();



    private final ArrayList<Unit> actiavtedUnits = new ArrayList<>();

    private final ArrayList<Fight> currentFights = new ArrayList<>();

    private final int unitSize;
    private final int maxAcctivatedUnits;


    public int x;
    public int y;

    private double accumelatedX = 0;
    private double accumelatedY = 0;



    private boolean canMove = true;

    /**
     * adds all units to the units list
     * and sets the center x, y pos
     * @param unitList List of units
     * @param x pos x
     * @param y pos y
     * @throws IllegalArgumentException if unitlsit is empty
     */
    public Legion(List<Unit> unitList, int x, int y, int unitSize) throws IllegalArgumentException{
        if (unitList.size() <= 0)
            throw new IllegalArgumentException("you cant create empty legian");
        this.x = x;
        this.y = y;
        this.units.addAll(unitList);
        this.unitSize = unitSize;
        maxAcctivatedUnits = Math.max((int) Math.sqrt((double)units.size()/3)*3, 1);
    }

    /**
     * move the enitre legian by an vector
     * @param x Xmove
     * @param y Ymove
     */
    public void moveByVektor(double x, double y){
        if (!canMove)
            return;

        if (Math.abs(x) <  1)
            accumelatedX += x;
        if (Math.abs(y) < 1)
            accumelatedY += y;

        if (Math.abs(accumelatedX) >= 1){
            x += (int) accumelatedX;
            accumelatedX = 0;
        }

        if (Math.abs(accumelatedY) >= 1){
            y += (int) accumelatedY;
            accumelatedY = 0;
        }

        int finalX = (int) x;
        int finalY = (int) y;
        this.x += finalX;
        this.y += finalY;

        if (actiavtedUnits.size() > 0){
            units.addAll(0, actiavtedUnits.stream().peek(unit -> {
                unit.targetX = 0;
                unit.targetY = 0;
            }).toList());
            actiavtedUnits.clear();
        }

        units.forEach(unit -> {
            unit.x += finalX;
            unit.y += finalY;
        });


    }

    /**
     * copys all units in legian and returns them
     * @return list of copied units
     */
    public List<Unit> getUnits(){
        return units.stream().map(this::copyUnit).toList();
    }


    /**
     * copys a unit
     * used multiple times in army class to remove references
     * @param unit unit to copy
     * @return copied unit
     */
    private Unit copyUnit(Unit unit){
        if (unit.getClass() == CavalryUnit.class)
            return new CavalryUnit(unit);
        else
        if (unit.getClass() == ComanderUnit.class)
            return new ComanderUnit(unit);
        else
        if (unit.getClass() == InfantryUnit.class)
            return new InfantryUnit(unit);
        else
        if (unit.getClass() == RangedUnit.class)
            return new RangedUnit(unit);
        return null;
    }


    /**
     * Checks if leagin is close to another
     * @param otherLegion leagin to check
     * @return true if they are close
     */
    public Boolean isLeagianClose(Legion otherLegion, int dir){
        boolean isClose = false;
        if (dir < 0)
            if (this.getClosestX(dir) <  otherLegion.getClosestX(-dir) + unitSize*4)
                isClose = true;
        if (dir > 0)
            if (this.getClosestX(dir) + unitSize*4 > otherLegion.getClosestX(-dir) )
                isClose = true;

        canMove = !isClose;
        otherLegion.setCanMove(canMove);
        return isClose;
    }

    /**
     * calculates how many units sto actiavte, then actiavtes them and sets their target
     * with setTargetOfActicvated method
     * @param otherLegion the legian to attack
     * @return true if every unit in enemy legian is gone
     */
    public boolean activateUnits(Legion otherLegion){
        if (otherLegion.getNumberOfTotalUnits() <= 0)
            return true;

        final int numberOfUnitsToAdd = Math.max(Math.min(
                Math.max(maxAcctivatedUnits, otherLegion.getNumberOfActivatedUnits())-actiavtedUnits.size(), units.size()), 0);
        for (int i = 0; i < numberOfUnitsToAdd ; i++) {
            actiavtedUnits.add(units.remove(0));

        }

        setTargetOfActivated(otherLegion);

        return false;
    }

    /**
     * moves the activated units towards the target
     * also dose simple collition detection
     * @param speed speed of units
     * @param deltaTime delta time for game loop
     */
    public void moveActivatedUnits(double speed, double deltaTime){
        actiavtedUnits.forEach(unit -> {

            //some collition detection
            if (unit.targetY != 0 || unit.targetX != 0){
                ArrayList<Double[]> forces = new ArrayList<>();
                this.actiavtedUnits.stream().filter(otherUnit->!unit.equals(otherUnit)).forEach(otherUnit->{
                    double otherUnitDist = Math.sqrt(Math.pow(otherUnit.x - unit.x, 2) + Math.pow(otherUnit.y - unit.y, 2));

                    if (otherUnitDist < (unitSize + 1)){
                        double dirX = (otherUnit.x - unit.x) / otherUnitDist;
                        double dirY = (otherUnit.y - unit.y) / otherUnitDist;
                        forces.add(new Double[]{dirX*speed, dirY*speed});
                    }
                });



                double finalX = 0;
                double finalY = 0;

                for (Double[] force : forces) {
                    finalX -= force[0]*deltaTime;
                    finalY -= force[1]*deltaTime;
                }

                //normalize the target vector
                double targetDistance = Math.sqrt(Math.pow(unit.x - unit.targetX, 2) + Math.pow(unit.y - unit.targetY, 2));
                double dirX = (unit.targetX - unit.x) / targetDistance;
                double dirY = (unit.targetY - unit.y) / targetDistance;

                if(targetDistance > 3){
                    finalX += dirX * speed * deltaTime;
                    finalY += dirY * speed * deltaTime;
                    //jiggle if stuck
                    if (finalX == 0 && finalY == 0){
                        finalX += speed * deltaTime + (Math.random() *2 -1)*3;
                    }
                    unit.moveByVektor(finalX, finalY);
                }
            }


    });
    }

    /**
     * sets the targets of the activated units
     * @param otherLegion legion to set target too;
     * @throws NullPointerException if map is not defined
     */
    public void setTargetOfActivated(Legion otherLegion) throws NullPointerException{
        if (otherLegion.actiavtedUnits.size() <= 0){
            return;
        }

        this.actiavtedUnits.forEach(unit -> {
            double closestDist = -1;
            Unit closestUnit = null;
            int maxHealth = 0;
            Unit unitWithMaxHealth = null;

            for (Unit targetUnit : otherLegion.actiavtedUnits) {
                double distToUnit = Math.sqrt(Math.pow(targetUnit.x - unit.x , 2) + Math.pow( targetUnit.y - unit.y , 2));
                if(!targetUnit.isInFight() || unit.isInFight()){
                    if (distToUnit < closestDist || closestDist == -1){
                        closestDist = distToUnit;
                        closestUnit = targetUnit;

                    }
                }
                else {
                    if (targetUnit.getHealth() > maxHealth){
                        maxHealth = targetUnit.getHealth();
                        unitWithMaxHealth = targetUnit;
                    }
                }
            }
            if (closestUnit == null) {
                if (unitWithMaxHealth != null){
                    unit.targetX = unitWithMaxHealth.x;
                    unit.targetY = unitWithMaxHealth.y;
                }

                return;
            }

            unit.targetX = closestUnit.x;
            unit.targetY = closestUnit.y;

            if (closestDist <= unitSize + 8){
                try {
                    if (!unit.isInFight() && !closestUnit.isInFight())
                        currentFights.add(new Fight(unit, closestUnit, GlobalData.getInstance().getTerrainFromColor(
                                GlobalData.getInstance().getMapImg().getPixelReader().getColor(unit.x, unit.y))));
                }catch (IllegalArgumentException | NullPointerException Ignored){
                    throw new NullPointerException("Cant create fight without map");
                }
            }
        });

    }

    /**
     * Calls fight method for every class
     * and finds units that lost fight if fight is done
     * @return list of units to remove
     */
    public List<Unit> fightAllFights(){

        ArrayList<Unit> unitsToRemove = new ArrayList<>();
        if(currentFights.size() <= 0)
            return unitsToRemove;

        ArrayList<Fight> fightsToRemove = new ArrayList<>();

        currentFights.stream().filter(Fight::attackEatchother).forEach(fight -> {
            unitsToRemove.add(fight.getLosingUnit());
            fight.getWinningUnit().setInFight(false);
            fightsToRemove.add(fight);
        });

        currentFights.removeAll(fightsToRemove);
        return unitsToRemove;
    }

    /**
     * checks if a unit is in the legain
     * @param u unit to check
     * @return true if it contains unit
     */
    public boolean contains(Unit u){
        return actiavtedUnits.contains(u) || units.contains(u);
    }

    /**
     * checks if unit is in legain
     * then removes it
     * @param u unit to remove
     * @return true if unit is remove false if unit is not found
     */
    public boolean removeUnit(Unit u){
        if (!contains(u))
            return false;

        if (actiavtedUnits.contains(u))
            actiavtedUnits.remove(u);
        else units.remove(u);

        return true;
    }

    /**
     * gets size of activated units array
     * @return number of activated untis
     */
    public int getNumberOfActivatedUnits(){
        return actiavtedUnits.size();
    }

    /**
     * gives the total number of units in army
     * @return number of units in army
     */
    public int getNumberOfTotalUnits(){
        return getNumberOfActivatedUnits() + units.size();
    }

    /**
     * decidec if the legian can move or not
     * @return true if can move
     */
    public boolean isCanMove() {
        return canMove;
    }

    /**
     * sets if legian can move or not
     * @param canMove true if legian can move
     */
    public void setCanMove(boolean canMove) {
        this.canMove = canMove;
    }


    /**
     * gets x value closest to the edge depening on direction of army
     * @param dir direction of army >0 is towards the right
     * @return the x value closest to edge
     */
    public int getClosestX(int dir){

        int largestX = (dir < 0) ? 1000 : -1000;
        if (actiavtedUnits.size() > 0){
            for (Unit unit : actiavtedUnits) {
                if (dir < 0)
                    if (unit.x < largestX){
                        largestX = unit.x;
                    }
                if (dir > 0)
                    if (unit.x > largestX){
                        largestX = unit.x;
                    }
            }

        }

        for (Unit unit : this.getUnits()) {
            if (dir < 0)
                if (unit.x < largestX){
                    largestX = unit.x;
                }
            if (dir > 0)
                if (unit.x > largestX){
                    largestX = unit.x;
                }
        }


        //There might not exist any units in legion for one frame so its set defualt value for legion
        if (largestX == 1000 || largestX == -1000)
            largestX = this.x;

        return largestX;
    }

    /**
     * returns copy of activated units array
     * @return activated untis
     */
    public List<Unit> getActiavtedUnits() {
        return actiavtedUnits.stream().map(this::copyUnit).toList();
    }
}
