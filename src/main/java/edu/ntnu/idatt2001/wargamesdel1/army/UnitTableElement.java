package edu.ntnu.idatt2001.wargamesdel1.army;

import edu.ntnu.idatt2001.wargamesdel1.util.UnitFactory;

/**
 * class for unit tables in armies stage
 */
public class UnitTableElement {


    private final String unitName;
    private final int unitNumber;
    private final String unitHealth;



    private final UnitFactory.UnitTypes unitType;


    /**
     * simple constructor
     * @param unitName name of unit
     * @param unitNumber number of units with that name
     * @param unitHealth
     * @param unitType
     */
    public UnitTableElement(String unitName, int unitNumber, String unitHealth, UnitFactory.UnitTypes unitType) {
        this.unitName = unitName;
        this.unitNumber = unitNumber;
        this.unitHealth = unitHealth;
        this.unitType = unitType;
    }

    /**
     * @return unit name
     */
    public String getUnitName() {
        return unitName;
    }

    /**
     * @return number of units
     */
    public int getUnitNumber() {
        return unitNumber;
    }

    /**
     * @return number of units
     */
    public String getUnitHealth() {
        return unitHealth;
    }

    /**
     * @return type of unit
     */
    public UnitFactory.UnitTypes getUnitType() {
        return unitType;
    }

}
