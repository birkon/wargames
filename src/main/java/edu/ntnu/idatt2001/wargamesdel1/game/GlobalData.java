package edu.ntnu.idatt2001.wargamesdel1.game;

import edu.ntnu.idatt2001.wargamesdel1.army.Army;
import edu.ntnu.idatt2001.wargamesdel1.units.Unit;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;

import java.util.HashMap;

/**
 * class to store currentArmy cross scenes
 * and change it
 */
public class GlobalData {

    private final static GlobalData GLOBAL_ARMIES_INSTANCE = new GlobalData();
    private Army currentArmy = null;
    private Army enemyArmy = null;



    private Army wonArmy = null;

    private Boolean didFriendlyWin = null;

    private Image mapImg = null;


    private HashMap<Color, Unit.Terrain> colorTerrainHashMap = new HashMap<>();

    /**
     * sets colors and terrain
     */
    public GlobalData(){
        colorTerrainHashMap.put(Color.BROWN, Unit.Terrain.FORREST);
        colorTerrainHashMap.put(Color.LIGHTGREEN, Unit.Terrain.PLAINS);
        colorTerrainHashMap.put(Color.DARKGRAY, Unit.Terrain.HILL);
    }

    /**
     * returns instance
     * @return instance of class
     */
    public static GlobalData getInstance() {
        return GLOBAL_ARMIES_INSTANCE;
    }

    /**
     * gets copy of the current army variable
     * @return current army
     */
    public Army getCurrentArmy() {
        try {
            return new Army(this.currentArmy);
        }catch (IllegalArgumentException e) {
            return null;
        }
    }

    /**
     * sets current army
     * @param currentArmy to set
     */
    public void setCurrentArmy(Army currentArmy) {
        this.currentArmy = currentArmy;
    }

    /**
     * gets copy of enemy army
     * @return
     */
    public Army getEnemyArmy() {
        try {
            return new Army(this.enemyArmy);
        }catch (IllegalArgumentException e) {
            return null;
        }
    }

    /**
     * gets terrain from color
     * @param c color
     * @return Terrain No terrain if color is null
     */
    public Unit.Terrain getTerrainFromColor(Color c){
        if (c == null)
            return Unit.Terrain.NO_TERRAIN;
        return colorTerrainHashMap.get(c);
    }

    /**
     * gets color from terrain
     * @param t terrain
     * @return Color
     */
    public Color getColorFromTerrain(Unit.Terrain t){
        if (!colorTerrainHashMap.containsValue(t))
            throw new IllegalArgumentException("terrain dose not exist");
        return colorTerrainHashMap.keySet().stream().filter(key-> colorTerrainHashMap.get(key).equals(t)).toList().get(0);
    }

    /**
     * sets enemy army
     *
     * @param enemyArmy
     */
    public void setEnemyArmy(Army enemyArmy) {
        this.enemyArmy = enemyArmy;
    }

    /**
     * gets the mapImage
     * @return mapImg
     */
    public Image getMapImg() {
        return mapImg;
    }

    /**
     * sets the map Img
     * @param mapImg
     */
    public void setMapImg(Image mapImg) {
        this.mapImg = mapImg;
    }

    /**
     * returns did friendly win variable
     * @return true if friendly army won
     */
    public Boolean getDidFriendlyWin() {
        return didFriendlyWin;
    }

    /**
     * set did friendly win
     * @param didFriendlyWin true if friendly army won
     */
    public void setDidFriendlyWin(Boolean didFriendlyWin) {
        this.didFriendlyWin = didFriendlyWin;
    }

    /**
     * return army that won
     * @return army that won
     */
    public Army getWonArmy() {
        return wonArmy;
    }

    /**
     * set winning army
     * @param wonArmy army that won
     */
    public void setWonArmy(Army wonArmy) {
        this.wonArmy = wonArmy;
    }


}
