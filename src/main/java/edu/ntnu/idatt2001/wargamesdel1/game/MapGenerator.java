package edu.ntnu.idatt2001.wargamesdel1.game;

import edu.ntnu.idatt2001.wargamesdel1.units.Unit;
import javafx.scene.image.Image;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

import java.util.Arrays;
import java.util.Random;

/**
 * generates map using noise
 * then creates img form it
 */
public class MapGenerator {

    private final int width;
    private final int height;

    private Color[][] colorArray;

    /**
     * generrates map
     * @param width width of map
     * @param height height of map
     * @param forrestThreshold threhold for forrest
     * @param mountainThreshold threshold for hills
     * @param seed seed to use for random
     * @param octaves number of octaves in noise
     * @param bias scale divider for later octaves to get more details
     */
    public MapGenerator(int width, int height, double forrestThreshold, double mountainThreshold, Long seed, int octaves, double bias){
        this.width = width;
        this.height = height;

        colorArray = new Color[width][height];
        colorArray = Arrays.stream(colorArray)
                .map(colors -> Arrays.stream(colors)
                        .map(color -> GlobalData.getInstance().getColorFromTerrain(Unit.Terrain.PLAINS))
                        .toArray(Color[]::new)).toArray(Color[][]::new);

        seed = (seed == null) ? System.currentTimeMillis() : seed;


        mapToTerrain(generateNoisWithThreshold(seed, forrestThreshold, octaves, bias), GlobalData.getInstance().getColorFromTerrain(Unit.Terrain.FORREST));
        mapToTerrain(generateNoisWithThreshold(seed + "mountain".hashCode(), mountainThreshold, octaves, bias), GlobalData.getInstance().getColorFromTerrain(Unit.Terrain.HILL));

    }

    /**
     * creates noise, then takes the threshold to create a 2d array with only 1 and 0
     * @param seed seed of the random value in Long
     * @param threshold number between 0 and 1
     * @param octaves number of octaves in noise
     * @param bias scale bias
     * @return 2d array of 1 and 0
     */
    private boolean[][] generateNoisWithThreshold(Long seed, double threshold, int octaves, double bias){
        boolean[][] thresholdArray = new boolean[width][height];
        double[] seedArray = new double[width*height];

        Random seededRand = new Random(seed);
        seedArray = Arrays.stream(seedArray).map(e->seededRand.nextDouble()).toArray();

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                double noise = 0.0;
                double scale = 1.0;
                double scaleAcc = 0;

                for (int o = 0; o < octaves; o++) {

                    int pitch = (Math.max(width, height))>> o;

                    int sampleX1 = (x / pitch) * pitch;
                    int sampleY1 = (y / pitch) * pitch;

                    int sampleX2 = (sampleX1 + pitch) % width;
                    int sampleY2 = (sampleY1 + pitch) % height;

                    double mixX = (double)(x-sampleX1) / (double)pitch;
                    double mixY = (double)(y-sampleY1) / (double)pitch;

                    double t = (1.0 - mixX) * seedArray[sampleX1 + sampleY1*width] + mixX * seedArray[sampleX2 + width*sampleY1];
                    double b = (1.0 - mixX) * seedArray[sampleX1 + width*sampleY2] + mixX * seedArray[sampleX2 + width*sampleY2];

                    noise += (mixY * (b - t) + t) * scale;
                    scaleAcc += scale;
                    scale = scale / bias;
                }
                thresholdArray[x][y] = noise / scaleAcc > threshold;

            }
        }
        return thresholdArray;
    }


    /**
     * Map colorArray with threshold array and color
     * @param booleanArray threshold array
     * @param color color to map to
     */
    private void mapToTerrain(boolean[][] booleanArray, Color color){
        for (int x = 0; x < booleanArray.length; x++) {
            for (int y = 0; y < booleanArray[x].length; y++) {
                if (booleanArray[x][y])
                    colorArray[x][y] = color;
            }
        }
    }

    /**
     * creates image from 2d array
     * @return Image object
     */
    public Image getImageOfMap(){
        WritableImage wImage = new WritableImage(width, height);
        PixelWriter writer = wImage.getPixelWriter();
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                writer.setColor(x, y, colorArray[x][y]);
            }
        }
        return wImage;
    }


}
