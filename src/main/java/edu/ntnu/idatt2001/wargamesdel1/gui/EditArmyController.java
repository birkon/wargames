package edu.ntnu.idatt2001.wargamesdel1.gui;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Optional;
import java.util.ResourceBundle;

import edu.ntnu.idatt2001.wargamesdel1.army.Army;
import edu.ntnu.idatt2001.wargamesdel1.army.UnitTableElement;
import edu.ntnu.idatt2001.wargamesdel1.game.GlobalData;
import edu.ntnu.idatt2001.wargamesdel1.util.UnitFactory;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.converter.IntegerStringConverter;

/**
 * controller for the edit army screen
 */
public class EditArmyController {
    //table data
    private final ObservableList<UnitTableElement> currrentArmieData = FXCollections.observableArrayList();

    //current unit table element selected
    private UnitTableElement selectedTabelElement = null;

    //unit factory
    UnitFactory unitFactory = new UnitFactory();

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TableColumn<?, ?> currentArmieNameCollum;

    @FXML
    private TableColumn<?, ?> currentArmieNumberCollum;

    @FXML
    private TableColumn<?, ?> currentArmieHealthCollum;

    @FXML
    private TableColumn<?, ?> currentArmieTypeCollom;

    @FXML
    private TableView<UnitTableElement> currentArmyTable;

    @FXML
    private Text armyName;

    @FXML
    private Spinner<Integer> numberSpinner;

    @FXML
    private Spinner<Integer> healthSpinner;

    @FXML
    private ChoiceBox<UnitFactory.UnitTypes> unitTypeBox;

    @FXML
    private TextField unitNameInput;

    /**
     * sets up the tables
     * and army name
     */
    @FXML
    void initialize() {

        unitTypeBox.getItems().addAll(UnitFactory.UnitTypes.CAVALRY, UnitFactory.UnitTypes.INFANTRY,
                UnitFactory.UnitTypes.COMMANDER, UnitFactory.UnitTypes.RANGED);
        unitTypeBox.setValue(UnitFactory.UnitTypes.INFANTRY);

        initializeSpinner();

        currentArmyTable.setEditable(true);

        currentArmieNameCollum.setCellValueFactory(new PropertyValueFactory<>("unitName"));
        currentArmieNumberCollum.setCellValueFactory(new PropertyValueFactory<>("unitNumber"));
        currentArmieHealthCollum.setCellValueFactory(new PropertyValueFactory<>("unitHealth"));
        currentArmieTypeCollom.setCellValueFactory(new PropertyValueFactory<>("unitType"));


        currentArmyTable.setItems(currrentArmieData);

        armyName.setText(GlobalData.getInstance().getCurrentArmy().getName());

        updateArmyTable(GlobalData.getInstance().getCurrentArmy());


        //add listener to selectedItemProperty of table view
        currentArmyTable.getSelectionModel().selectedItemProperty().addListener(((observableValue, oldValue, newValue) -> {
            selectedTabelElement = newValue;
            updateSelectedUnitEditor();
        }));
    }

    /**
     * method to initialize the spinners
     */
    private void initializeSpinner() {
        // Value factory.
        SpinnerValueFactory<Integer> valueFactoryNumber =
                new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 1000, 5);

        numberSpinner.setValueFactory(valueFactoryNumber);
        numberSpinner.setEditable(true);
        numberSpinner.getEditor().setTextFormatter(new TextFormatter<>(new IntegerStringConverter(), 5));


        // Value factory.
        SpinnerValueFactory<Integer> valueFactoryHealth =
                new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 1000, 100);

        healthSpinner.setValueFactory(valueFactoryHealth);
        healthSpinner.setEditable(true);
        healthSpinner.getEditor().setTextFormatter(new TextFormatter<>(new IntegerStringConverter(), 100));
    }

    /**
     * Creates a dialog box with text input
     * if result is not given it will retrun empty string
     * @return string result, or empty string if none is given
     */
    private String createArmyNameDialog(){
        TextInputDialog dialog = new TextInputDialog("");
        dialog.setTitle("Army name");
        dialog.setHeaderText("select a name for the army");
        dialog.setContentText("Please enter the army name:");

        Optional<String> result = dialog.showAndWait();
        return result.orElse("");

    }


    /**
     * Gives a prompt to update the army name.
     * and updates it
     */
    @FXML
    public void updateArmyName(){
        String newName = createArmyNameDialog();
        if(newName.isEmpty()){
            Optional<ButtonType> alertResult = GuiUtil.createAlert("You have to enter a name");
            if (alertResult.isPresent() && alertResult.get() == ButtonType.OK){
                updateArmyName();
            }
            return;
        }

        Army copyArmy = GlobalData.getInstance().getCurrentArmy();
        copyArmy.setName(newName);
        GlobalData.getInstance().setCurrentArmy(copyArmy);

        armyName.setText(newName);
    }

    /**
     * updates table view to army data
     * @param army army to update to
     */
    private void updateArmyTable(Army army){
        currrentArmieData.clear();
        if (army == null)
            return;
        currrentArmieData.addAll(army.getUnitTableElements(true));
    }

    /**
     * loads selected armies screen
     * @param event
     */
    @FXML
    public void loadSelectArmiesScreen(ActionEvent event){
        try{
            Scene scene = new Scene(FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/selectArmiesScreen.fxml"))), 1071, 626);
            Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
            stage.setScene(scene);
            stage.show();
        }catch (NullPointerException | IOException e){
            e.printStackTrace();
        }
    }

    /**
     * addes unit to army
     */
    @FXML
    public void addUnitToArmy(){

        if (unitNameInput.getText().isEmpty()){
            GuiUtil.createAlert("you need to select unit name");
            return;
        }

        Army copyOfArmy = GlobalData.getInstance().getCurrentArmy();

        if (selectedTabelElement != null){
            copyOfArmy.remove(copyOfArmy.getAllUnitsOfUnitTableElement(selectedTabelElement));
            selectedTabelElement = null;

        }

        copyOfArmy.addAll(new ArrayList<>(unitFactory.CreateMultipleUnits(unitTypeBox.getValue(), unitNameInput.getText(),
                healthSpinner.getValue(), numberSpinner.getValue())));

        GlobalData.getInstance().setCurrentArmy(copyOfArmy);

        updateArmyTable(copyOfArmy);
        updateSelectedUnitEditor();
    }

    /**
     * Updates the promt text for the different fields
     */
    private void updateSelectedUnitEditor(){
        if (selectedTabelElement != null){
            unitNameInput.setText(selectedTabelElement.getUnitName());
            unitTypeBox.setValue(selectedTabelElement.getUnitType());
            unitTypeBox.setDisable(true);
            numberSpinner.editorProperty().getValue().setText(String.valueOf(selectedTabelElement.getUnitNumber()));
            healthSpinner.editorProperty().getValue().setText(String.valueOf(selectedTabelElement.getUnitHealth()));
            return;
        }
        unitNameInput.setText("");
        unitTypeBox.setValue(UnitFactory.UnitTypes.INFANTRY);
        unitTypeBox.setDisable(false);
        numberSpinner.editorProperty().getValue().setText(String.valueOf(5));
        healthSpinner.editorProperty().getValue().setText(String.valueOf(100));
    }

    /**
     * remove unit/units from army
     * it removes the selected unit
     * and creates and alert if no unit is selected
     */
    @FXML
    public void removeUnit(){
        if (selectedTabelElement == null){
            GuiUtil.createAlert("you have to select unit to remove it");
            return;
        }

        Army copyOfArmy = GlobalData.getInstance().getCurrentArmy();

        copyOfArmy.remove(copyOfArmy.getAllUnitsOfUnitTableElement(selectedTabelElement));

        GlobalData.getInstance().setCurrentArmy(copyOfArmy);

        updateArmyTable(copyOfArmy);
        updateSelectedUnitEditor();
    }

}
