package edu.ntnu.idatt2001.wargamesdel1.gui;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.ResourceBundle;

import edu.ntnu.idatt2001.wargamesdel1.army.Army;
import edu.ntnu.idatt2001.wargamesdel1.army.Legion;
import edu.ntnu.idatt2001.wargamesdel1.game.GlobalData;
import edu.ntnu.idatt2001.wargamesdel1.units.Unit;
import javafx.animation.AnimationTimer;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.shape.ArcType;
import javafx.stage.Stage;

/**
 * controller for game screen
 * controlls what to do and when in the game
 * also takes inputs on button actions
 */
public class GameController {

    private double gameSpeed = 1; //speed of the game
    private int unitSize; //base size of units

    private Army friendlyArmy; //friendly army (to the left)
    private Army enemyArmy; //enemy army to the right
    private final ArrayList<Legion> friendlyLegions = new ArrayList<>(); //friendly legians
    private final ArrayList<Legion> enemyLegions = new ArrayList<>(); //enemy legians


    //graphiccontext for canvas element to wrinte units too
    private GraphicsContext gCanvas;

    //global id used for identifying units created in factory
    private int unitIds = 0;

    //settings for attacks pr second
    private double attackTimer = 0.0; //timer for attackspeed
    private final double attacksASecond = 2; //attacks a second

    private AnimationTimer gameLoop;

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private ImageView mainScreen;

    @FXML
    private Canvas unitScreen;

    @FXML
    private Button playButton;

    @FXML
    private Button gameSpeedButton;

    /**
     * method for loading the main menue of game.
     * all global data is the same as when started game
     * becoues all GlobalData methods give deepCopys
     * @param event
     */
    @FXML
    void goBackButton(ActionEvent event){
        loadScene((Stage) ((Node) event.getSource()).getScene().getWindow(), "/gameMenu.fxml");
    }

    /**
     * Method for loading scene to stage
     * @param stage stage to set screen to
     * @param pathToScene Scene to load
     */
    private void loadScene( Stage stage, String pathToScene){

        try {
            Scene scene = new Scene(FXMLLoader.load(Objects.requireNonNull(getClass().getResource(pathToScene))), 1071, 626);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            throw new IllegalArgumentException("Scene could not be loaded");
        }

    }


    /**
     * sets up amries and starts game loop
     */
    @FXML
    void initialize() {

        //sets values from globalData set in previous screens
        mainScreen.setImage(GlobalData.getInstance().getMapImg());
        friendlyArmy = GlobalData.getInstance().getCurrentArmy();
        enemyArmy = GlobalData.getInstance().getEnemyArmy();

        gCanvas = unitScreen.getGraphicsContext2D();


        //sets the defualt unit size based on number of units
        int allUnits = friendlyArmy.getAllUnits().size() + enemyArmy.getAllUnits().size();
        if (allUnits>30000)
            unitSize = 1;
        else if (allUnits > 5000)
            unitSize = 2;
        else if (allUnits > 1000)
            unitSize = 4;
        else if (allUnits > 500)
            unitSize = 6;
        else if (allUnits > 200)
            unitSize = 8;
        else
            unitSize = 10;


        placeUnits();
        draw(); //initial draw


        //set button action for gameSpeedButton
        gameSpeedButton.setText(String.valueOf(gameSpeed) + "x");
        gameSpeedButton.setOnAction((event)->{
            if (gameSpeed < 3){
                gameSpeed += 0.5;
            }else gameSpeed = 1;
            gameSpeedButton.setText(String.valueOf(gameSpeed) + "x");
        });


        //creates game loop by using animationTimer
        gameLoop = new AnimationTimer(){
            @Override
            public void handle(long nanoTimer) {
                double deltaT = 1.0/60.0;
                update(deltaT*gameSpeed);
            }
        };

        //set start and stop game loop to button
        playButton.setOnAction((event)->{
            boolean isGameLoopRunning = playButton.getText().equalsIgnoreCase("play");
            playButton.setText((isGameLoopRunning ? "Pause" : "Play"));

            if (isGameLoopRunning)
                gameLoop.start();
            else gameLoop.stop();
        });


    }


    /**
     * the game loop
     * @param deltaT time between frames
     */
    private void update(double deltaT){

        if (friendlyLegions.size() <= 0){
            GlobalData.getInstance().setDidFriendlyWin(false);
            GlobalData.getInstance().setWonArmy(enemyArmy);
            loadScene((Stage)mainScreen.getScene().getWindow(), "/postGameScreen.fxml");
            gameLoop.stop();
            return;
        }else if (enemyLegions.size() <= 0){
            GlobalData.getInstance().setDidFriendlyWin(true);
            GlobalData.getInstance().setWonArmy(friendlyArmy);
            loadScene((Stage)mainScreen.getScene().getWindow(), "/postGameScreen.fxml");
            gameLoop.stop();
            return;
        }



        if (friendlyLegions.get(0).isCanMove())
            friendlyLegions.forEach(legion -> legion.moveByVektor(50*deltaT, 0));

        if (enemyLegions.get(0).isCanMove())
            enemyLegions.forEach(legion -> legion.moveByVektor(-50*deltaT, 0));

        if (friendlyLegions.get(0).isLeagianClose(enemyLegions.get(0), 1)){
            if(friendlyLegions.get(0).activateUnits(enemyLegions.get(0))){
                enemyLegions.remove(0);
            }else if (enemyLegions.get(0).activateUnits(friendlyLegions.get(0)))
                friendlyLegions.remove(0);
        }

        friendlyLegions.forEach(legion -> legion.moveActivatedUnits(20, deltaT));
        enemyLegions.forEach(legion -> legion.moveActivatedUnits(20, deltaT));

        fightAllFights(deltaT);

        draw();
    }

    /**
     * calls the fightAllFights method in every legion
     * also removes dead units from legaians and army
     * @param deltaT delta time for timer
     */
    private void fightAllFights(double deltaT){
        attackTimer += deltaT;
        if (attackTimer >= (1.0/attacksASecond)){
            Legion currentlyFriendlyLegion = friendlyLegions.get(0);
            Legion currentlyEnemyLegion = enemyLegions.get(0);

            for(Legion currentLegion : new Legion[]{currentlyFriendlyLegion, currentlyEnemyLegion}) {
                ArrayList<Unit> unitsToRemove = new ArrayList<>(currentLegion.fightAllFights());
                unitsToRemove.forEach(unit -> {
                    friendlyArmy.remove(unit);
                    enemyArmy.remove(unit);
                    currentlyFriendlyLegion.removeUnit(unit);
                    currentlyEnemyLegion.removeUnit(unit);
                });

            }

            attackTimer = 0;
        }

    }

    /**
     * draws the units to canvas
     */
    private void draw(){
        gCanvas.clearRect(0, 0, 900, 500);


        friendlyArmy.getAllUnits().forEach(unit -> {
            gCanvas.beginPath();
            gCanvas.fillArc(unit.x, unit.y, unitSize, unitSize, 0 , 360, ArcType.OPEN);
        });
        enemyArmy.getAllUnits().forEach(unit -> {
            gCanvas.beginPath();
            gCanvas.fillArc(unit.x, unit.y, unitSize, unitSize, 0 , 360, ArcType.OPEN);
        });

    }

    /**
     * Calls the placeArmy method on both armies
     */
    private void placeUnits(){
        placeArmy(friendlyArmy, unitSize, -1, (int) (900/2.7), (int)(500/2), friendlyLegions);
        placeArmy(enemyArmy, unitSize, 1, (int) (900/1.5), (int)(500/2), enemyLegions);
    }

    /**
     * gives the units their x and y initial positions
     * @param army army to position
     * @param spacing spacing between units
     * @param direction direction the units to face
     * @param xCenter center x pos
     * @param yCenter center y pos
     * @param listOfLegions list to add legain to (exp. friendlyLegains)
     */
    private void placeArmy(Army army, int spacing, int direction, int xCenter, int yCenter, ArrayList<Legion> listOfLegions){
        direction = (direction > 0) ? 1 : -1;
        int betweenTypesSplitting = direction*spacing*3;

        army.getAllUnits().forEach(unit -> {
            unit.setId(unitIds);
            unitIds += 1;
        });
        int lastX = xCenter;
        if(army.getInfatryUnits().size() >0) {
            lastX = setPosFromCenter(lastX, yCenter, army.getInfatryUnits(), spacing, direction);
            listOfLegions.add(new Legion(army.getInfatryUnits(), xCenter, yCenter, unitSize));
        }
        if(army.getRangedUnit().size() >0){
            lastX = setPosFromCenter(lastX + betweenTypesSplitting, yCenter, army.getRangedUnit(), spacing, direction);
            listOfLegions.add(new Legion(army.getRangedUnit(), lastX + betweenTypesSplitting, yCenter, unitSize));
        }


        if(army.getCavalryUnits().size() >0){
            lastX = setPosFromCenter(lastX + betweenTypesSplitting, yCenter, army.getCavalryUnits(), spacing, direction);
            listOfLegions.add(new Legion(army.getCavalryUnits(), lastX + betweenTypesSplitting, yCenter, unitSize));

        }
        if(army.getComanderUnit().size() >0){
            setPosFromCenter(lastX + betweenTypesSplitting, yCenter, army.getComanderUnit(), spacing, direction);
            listOfLegions.add(new Legion(army.getComanderUnit(), lastX + betweenTypesSplitting, yCenter, unitSize));
        }



    }

    /**
     * sets the position of the units in inputed list
     * @param x x Center
     * @param y y center
     * @param units list of units
     * @param splittingDistance distance between units
     * @param dir direction
     * @return last x position
     */
    private int setPosFromCenter(int x, int y, List<Unit> units, int splittingDistance, int dir){

        int colloms = Math.max((int) Math.sqrt((double)units.size()/3), 1);
        int rows = Math.max(colloms*3, 1);
        double lengthOfRows = rows * splittingDistance;
        int startY = y + (int) lengthOfRows/2;
        int currY = startY;
        int currX = x;

        for (int i = 0; i < units.size(); i++) {
            Unit currentUnit = units.get(i);
            currentUnit.y = currY;
            currentUnit.x = currX;
            currY -= splittingDistance;

            if((i + 1)  % rows == 0 && i != 0){
                currY = startY;
                currX += dir*splittingDistance;
            }
        }
        return currX;
    }


}

