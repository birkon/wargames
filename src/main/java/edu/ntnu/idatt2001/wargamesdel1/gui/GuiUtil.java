package edu.ntnu.idatt2001.wargamesdel1.gui;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.util.Optional;

/**
 * utility methods used in controllers
 */
public class GuiUtil {
    /**
     * creates alert and displays it
     * @param message message to display
     */
    public static Optional<ButtonType> createAlert(String message) {

        Alert a = new Alert(Alert.AlertType.CONFIRMATION);

        a.setContentText(message);

        return a.showAndWait();
    }

}
