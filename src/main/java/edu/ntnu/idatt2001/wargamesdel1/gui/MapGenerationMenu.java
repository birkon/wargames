package edu.ntnu.idatt2001.wargamesdel1.gui;

import java.io.IOException;
import java.net.URL;
import java.util.Objects;
import java.util.ResourceBundle;

import edu.ntnu.idatt2001.wargamesdel1.game.GlobalData;
import edu.ntnu.idatt2001.wargamesdel1.game.MapGenerator;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;


/**
 * Controller for generate map screen
 * main functionality is taking input values for GenerateMap class
 */
public class MapGenerationMenu {

    /**
     * change listener for updating preview image on slider change
     */
     private class changeSliderHandler implements ChangeListener<Number>{
         @Override
         public void changed(ObservableValue<? extends Number> observableValue, Number number, Number t1) {
             if (sliderPreviewUpdate)
                 generateMap();
         }
     }

     //preivew for map generation might lag if turned on.
    boolean sliderPreviewUpdate = false;

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField seedTextField;

    @FXML
    private ImageView previewIMG;

    @FXML
    private Slider octaveSlider;

    @FXML
    private Slider scaleSlider;

    @FXML
    private Slider forrestThresholdSlider;

    @FXML
    private Slider mauntainThresholdSlider;

    @FXML
    private CheckBox preivewUpdateBox;

    /**
     * loads main menu
     * @param event
     */
    @FXML
    void switchToMenu(ActionEvent event) {
        loadScene(event, "/gameMenu.fxml");
    }

    /**
     * loads selectEnemy screen
     * @param event
     */
    @FXML
    void switchToSelectEnemy(ActionEvent event) {
        loadScene(event, "/selectEnemy.fxml");
    }

    /**
     * loads game screen
     * @param event
     */
    @FXML
    void switchToGame(ActionEvent event) {
        if (GlobalData.getInstance().getCurrentArmy() != null && GlobalData.getInstance().getMapImg() != null
                && GlobalData.getInstance().getEnemyArmy() != null)
                    loadScene(event, "/game.fxml");
    }


    /**
     * generates map from input values
     */
    @FXML
    void generateMap(){
        MapGenerator mp = new MapGenerator(900, 500, forrestThresholdSlider.getValue(),  mauntainThresholdSlider.getValue(),
                (seedTextField.getText().isEmpty()) ? null : (long)seedTextField.getText().hashCode(),
                (int)octaveSlider.getValue(), scaleSlider.getValue());
        Image img = mp.getImageOfMap();
        GlobalData.getInstance().setMapImg(img);
        previewIMG.setImage(img);
    }


    /**
     * sets up change listeners for sliders and text.
     * also sets img if it was has already been generated onc
     */
    @FXML
    void initialize() {
        assert seedTextField != null : "fx:id=\"seedTextField\" was not injected: check your FXML file 'generateMapMenue.fxml'.";

        mauntainThresholdSlider.valueProperty().addListener(new changeSliderHandler());
        forrestThresholdSlider.valueProperty().addListener(new changeSliderHandler());
        octaveSlider.valueProperty().addListener(new changeSliderHandler());
        scaleSlider.valueProperty().addListener(new changeSliderHandler());
        preivewUpdateBox.selectedProperty().addListener((observableValue, aBoolean, t1) -> {
            sliderPreviewUpdate = !sliderPreviewUpdate;
        });

        previewIMG.setPreserveRatio(false);
        if(GlobalData.getInstance().getMapImg() != null)
            previewIMG.setImage(GlobalData.getInstance().getMapImg());

    }

    /**
     * Method for loading scene to stage
     * @param event actionevent from buttonclick
     * @param pathToScene Scene to load
     */
    private void loadScene(ActionEvent event, String pathToScene){

        try {
            Scene scene = new Scene(FXMLLoader.load(Objects.requireNonNull(getClass().getResource(pathToScene))), 1071, 626);
            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
            throw new IllegalArgumentException("Scene could not be loaded");
        }

    }

}
