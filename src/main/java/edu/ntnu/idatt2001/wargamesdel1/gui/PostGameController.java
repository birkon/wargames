package edu.ntnu.idatt2001.wargamesdel1.gui;

import edu.ntnu.idatt2001.wargamesdel1.army.Army;
import edu.ntnu.idatt2001.wargamesdel1.army.UnitTableElement;
import edu.ntnu.idatt2001.wargamesdel1.game.GlobalData;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Objects;

/**
 * conteroller class for the post game screen
 */
public class PostGameController {

    //for storing table data for tableview
    private final ObservableList<UnitTableElement> currrentArmieData = FXCollections.observableArrayList();

    //instance of globalData class
    private final GlobalData globalData = GlobalData.getInstance();

    private Army armyToSaveToCurrent;

    @FXML
    private TableColumn<?, ?> currentArmieHealthCollum;

    @FXML
    private TableColumn<?, ?> currentArmieNameCollum;

    @FXML
    private TableColumn<?, ?> currentArmieNumberCollum;

    @FXML
    private TableColumn<?, ?> currentArmieTypeCollom;

    @FXML
    private TableView<UnitTableElement> currentArmyTable;


    @FXML
    private Text wonLostText;

    @FXML
    private Text armyNameText;

    /**
     * loads back to main menue
     * also saves army to current if button was pressed
     * @param event
     */
    @FXML
    void backToMenue(ActionEvent event) {
        if (armyToSaveToCurrent != null)
            globalData.setCurrentArmy(armyToSaveToCurrent);
        try{
            Scene scene = new Scene(FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/gameMenu.fxml"))), 1071, 626);
            Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
            stage.setScene(scene);
            stage.show();
        }catch (NullPointerException | IOException e){
            e.printStackTrace();
        }
    }

    /**
     * Sets armyToSaveToCurrent to won army
     * @param event
     */
    @FXML
    void selectWonArmy(ActionEvent event) {
        armyToSaveToCurrent = globalData.getWonArmy();
    }

    /**
     * restocks the won army to army pre battle
     * @param event
     */
    @FXML
    void restockArmy(ActionEvent event) {
        globalData.setWonArmy((globalData.getDidFriendlyWin()) ? globalData.getCurrentArmy() : globalData.getEnemyArmy());
        updateArmyTable(globalData.getWonArmy());
    }

    /**
     * sets up tables
     */
    @FXML
    void initialize(){
        wonLostText.setText((globalData.getDidFriendlyWin()) ? "You won!" : "You lost!");
        armyNameText.setText(globalData.getWonArmy().getName());

        currentArmyTable.setEditable(true);

        currentArmieNameCollum.setCellValueFactory(new PropertyValueFactory<>("unitName"));
        currentArmieNumberCollum.setCellValueFactory(new PropertyValueFactory<>("unitNumber"));
        currentArmieHealthCollum.setCellValueFactory(new PropertyValueFactory<>("unitHealth"));
        currentArmieTypeCollom.setCellValueFactory(new PropertyValueFactory<>("unitType"));

        currentArmyTable.setItems(currrentArmieData);
        updateArmyTable(globalData.getWonArmy());

    }

    /**
     * updates table view to army data
     * @param army army to update to
     */
    private void updateArmyTable(Army army){

        currrentArmieData.clear();
        if (army == null)
            return;
        currrentArmieData.addAll(army.getUnitTableElements(true));
    }


}
