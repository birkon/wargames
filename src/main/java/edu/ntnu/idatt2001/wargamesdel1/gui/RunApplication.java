package edu.ntnu.idatt2001.wargamesdel1.gui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Main class for loading first scene
 */
public class RunApplication extends Application {
    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(RunApplication.class.getResource("/gameMenu.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 1071, 626);
        stage.setTitle("WarGames");
        stage.setScene(scene);
        stage.show();
    }


    /**
     * launches the app
     * @param args
     */
    public static void main(String[] args) {
        launch();
    }
}