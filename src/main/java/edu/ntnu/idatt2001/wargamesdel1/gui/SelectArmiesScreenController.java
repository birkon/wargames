package edu.ntnu.idatt2001.wargamesdel1.gui;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;
import java.util.ResourceBundle;
import edu.ntnu.idatt2001.wargamesdel1.army.Army;
import edu.ntnu.idatt2001.wargamesdel1.army.UnitTableElement;
import edu.ntnu.idatt2001.wargamesdel1.game.GlobalData;
import edu.ntnu.idatt2001.wargamesdel1.util.DataHandler;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * Controller for selectArmies fxml
 * selects and loads armies to globalData
 */
public class SelectArmiesScreenController {

    //current clicked node
    private Node clickedNode = null;

    //data for tables
    private final ObservableList<UnitTableElement> currentArmieData = FXCollections.observableArrayList();
    private final ObservableList<UnitTableElement> selectedArmieData = FXCollections.observableArrayList();

    //GlobalData instance
    private final GlobalData globalData = GlobalData.getInstance();

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private VBox saveTable;

    @FXML
    private TableView<UnitTableElement> selectedArmyTable;

    @FXML
    private TableView<UnitTableElement> currentArmyTable;

    @FXML
    private TableColumn<UnitTableElement, String> currentArmieNameCollum;

    @FXML
    private TableColumn<UnitTableElement, Integer> currentArmieNumberCollum;

    @FXML
    private TableColumn<UnitTableElement, String> selectedArmieNameCollum;

    @FXML
    private TableColumn<UnitTableElement, Integer> selectedArmieNumberCollum;

    @FXML
    private Text currentArmyName;

    @FXML
    private Text selectedArmyName;


    /**
     * sets up the tabels, then calls the initial setup methods for them
     */
    @FXML
    void initialize() {

        selectedArmyTable.setEditable(true);
        currentArmyTable.setEditable(true);

        currentArmieNameCollum.setCellValueFactory(new PropertyValueFactory<>("unitName"));
        currentArmieNumberCollum.setCellValueFactory(new PropertyValueFactory<>("unitNumber"));

        selectedArmieNameCollum.setCellValueFactory(new PropertyValueFactory<>("unitName"));
        selectedArmieNumberCollum.setCellValueFactory(new PropertyValueFactory<>("unitNumber"));


        currentArmyTable.setItems(currentArmieData);
        selectedArmyTable.setItems(selectedArmieData);

        setUpTabel();
        try {
            updateArmyTable(globalData.getCurrentArmy(), currentArmieData, currentArmyName);
        } catch (IllegalArgumentException e) {
            updateArmyTable(null, currentArmieData, currentArmyName);
        }


    }

    /**
     * creates elements for every saveName and adds them to vbox
     */
    private void setUpTabel() {
        saveTable.getChildren().clear();

        Arrays.stream(DataHandler.getAllSaveNames()).forEach(e ->
                saveTable.getChildren().add(
                createPaneTextNode(e)
        ));
    }

    /**
     * create vbox elemennt from save name
     *
     * @param name of save
     * @return node element to be added
     */
    private Node createPaneTextNode(String name) {
        Pane pane = new Pane();
        pane.prefHeight(Region.USE_COMPUTED_SIZE);
        pane.prefWidth(Region.USE_COMPUTED_SIZE);
        pane.getStyleClass().add("pane");


        Text text = new Text();
        text.setText(name);
        text.setWrappingWidth(280);
        text.setLayoutX(5);
        text.setLayoutY(20);

        text.setId("saveNameText");

        pane.getChildren().add(text);

        //add on click event to pane elements
        pane.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> updateClickedNode(pane));

        return pane;
    }

    /**
     * updates border color, and current selected node used when displaying selected army
     *
     * @param newClickedNode clicked node
     */
    private void updateClickedNode(Node newClickedNode) {

        if (clickedNode != null)
            this.clickedNode.setStyle("-fx-border-color: gray");

        this.clickedNode = newClickedNode;


        if (clickedNode != null) {
            this.clickedNode.setStyle("-fx-border-color: green");

            updateArmyTable(DataHandler.load(getSaveNameFromCurrentNode()), selectedArmieData, selectedArmyName);

        } else {
            updateArmyTable(null, selectedArmieData, selectedArmyName);
        }

    }

    /**
     * updates the data lists witch in turn updates the tables
     *
     * @param army         army to update to
     * @param data         Data list to update
     * @param armyNameNode Army name Text element
     */
    private void updateArmyTable(Army army, ObservableList<UnitTableElement> data, Text armyNameNode) {
        data.clear();
        armyNameNode.setText("Name:");
        if (army == null)
            return;

        data.addAll(army.getUnitTableElements(false));
        armyNameNode.setText(army.getName());
    }


    /**
     * save current army.
     * called on action in save button
     * if it fails it will show an error message
     */
    public void saveCurrentArmy() {
        Army armyToSave = globalData.getCurrentArmy();
        if (armyToSave == null)
            GuiUtil.createAlert("Select army before saving");
        else {
            try {
                String saveName = createSaveNameDialogInput();
                if (Arrays.stream(DataHandler.getAllSaveNames()).anyMatch(s -> s.equalsIgnoreCase(saveName))) {
                    Optional<ButtonType> alertResult = GuiUtil.createAlert("This will override existing save");
                    if (alertResult.isPresent() && alertResult.get() == ButtonType.OK) {
                        DataHandler.save(saveName, armyToSave);
                    }

                } else
                    DataHandler.save(saveName, armyToSave);
                setUpTabel();
            } catch (IllegalArgumentException e) {
                GuiUtil.createAlert(e.getMessage());
            }

        }

    }


    /**
     * Creates a dialog box with text input
     * if result is not given it will retrun empty string
     *
     * @return string result, or empty string if none is given
     */
    private String createSaveNameDialogInput() {
        TextInputDialog dialog = new TextInputDialog("");
        dialog.setTitle("saving");
        dialog.setHeaderText("select a name for the save");
        dialog.setContentText("Please enter the save name:");

        Optional<String> result = dialog.showAndWait();
        return result.orElse("");

    }

    /**
     * checks if army is selected
     * if it is then loads it to current army
     */
    public void loadSelectedArmy() {
        if (clickedNode == null) {
            GuiUtil.createAlert("select army before loading");
            return;
        }

        globalData.setCurrentArmy(
                DataHandler.load(getSaveNameFromCurrentNode()));
        updateClickedNode(null);

        updateArmyTable(globalData.getCurrentArmy(), currentArmieData, currentArmyName);
    }

    public void deleteSelectedArmy() {
        if (clickedNode == null) {
            GuiUtil.createAlert("select army before deleting");
            return;
        }

        if (!DataHandler.deleteSave(getSaveNameFromCurrentNode()))
            GuiUtil.createAlert("could not delete army");

        updateClickedNode(null);
        setUpTabel();
    }

    /**
     * casts nodes to correct classes and finds text node
     *
     * @return text from text node
     * @throws RuntimeException if newClickedNode is of wrong foramt
     */
    private String getSaveNameFromCurrentNode() throws RuntimeException {
        try {
            return ((Text) (((Pane) clickedNode).getChildren().get(0))).getText();
        } catch (ClassCastException e) {
            throw new RuntimeException("current clicked node is of wrong format");
        }
    }

    /**
     * Method for loading scene to stage
     * @param event actionevent from buttonclick
     * @param pathToScene Scene to load
     */
    private void loadScene(ActionEvent event, String pathToScene){

        try {
            Scene scene = new Scene(FXMLLoader.load(Objects.requireNonNull(getClass().getResource(pathToScene))), 1071, 626);
            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            throw new IllegalArgumentException("Scene could not be loaded");
        }

    }

    /**
     * loads main menue fxml
     * @param event
     */
    @FXML
    public void loadMainMenu(ActionEvent event) {
        loadScene(event, "/gameMenu.fxml");

    }


    /**
     * loads edit army fxml
     * gives popup if no army is selected
     * @param event
     */
    @FXML
    public void loadEditArmmyScreen(ActionEvent event){
        if(GlobalData.getInstance().getCurrentArmy() == null){
            GlobalData.getInstance().setCurrentArmy(new Army("select name"));
        }
        loadScene(event, "/editArmy.fxml");
    }
}
