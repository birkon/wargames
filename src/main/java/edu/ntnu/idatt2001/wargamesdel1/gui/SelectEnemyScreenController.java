package edu.ntnu.idatt2001.wargamesdel1.gui;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.Objects;
import java.util.ResourceBundle;

import edu.ntnu.idatt2001.wargamesdel1.army.Army;
import edu.ntnu.idatt2001.wargamesdel1.army.UnitTableElement;
import edu.ntnu.idatt2001.wargamesdel1.game.GlobalData;
import edu.ntnu.idatt2001.wargamesdel1.util.DataHandler;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * Controller for select enemy screen
 */
public class SelectEnemyScreenController {

    //clicked table element
    private Node clickedNode = null;

    //table data
    private final ObservableList<UnitTableElement> selectedArmieData = FXCollections.observableArrayList();

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private VBox saveTable;

    @FXML
    private TableColumn<UnitTableElement, String> selectedArmieNameCollum;

    @FXML
    private TableColumn<UnitTableElement, Integer> selectedArmieNumberCollum;

    @FXML
    private Text selectedArmyName;

    @FXML
    private TableView<UnitTableElement> selectedArmyTable;

    /**
     * goes to mapGenerationMenue
     * @param event
     */
    @FXML
    void loadMapMenue(ActionEvent event) {
        if (clickedNode != null)
            GlobalData.getInstance().setEnemyArmy(DataHandler.load(getSaveNameFromCurrentNode()));
        loadScene(event, "/generateMapMenue.fxml");

    }

    /**
     * sets up tablels and event listeners
     */
    @FXML
    void initialize() {

        selectedArmyTable.setEditable(true);


        selectedArmieNameCollum.setCellValueFactory(new PropertyValueFactory<>("unitName"));
        selectedArmieNumberCollum.setCellValueFactory(new PropertyValueFactory<>("unitNumber"));

        selectedArmyTable.setItems(selectedArmieData);

        setUpTabel();

        try {
            updateArmyTable(GlobalData.getInstance().getEnemyArmy(), selectedArmieData, selectedArmyName);
        } catch (IllegalArgumentException e) {
            updateArmyTable(null, selectedArmieData, selectedArmyName);
        }
    }

    /**
     * Method for loading scene to stage
     * @param event actionevent from buttonclick
     * @param pathToScene Scene to load
     */
    private void loadScene(ActionEvent event, String pathToScene){

        try {
            Scene scene = new Scene(FXMLLoader.load(Objects.requireNonNull(getClass().getResource(pathToScene))), 1071, 626);
            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            throw new IllegalArgumentException("Scene could not be loaded");
        }

    }

    /**
     * create vbox elemennt from save name
     *
     * @param name of save
     * @return node element to be added
     */
    private Node createPaneTextNode(String name) {
        Pane pane = new Pane();
        pane.prefHeight(Region.USE_COMPUTED_SIZE);
        pane.prefWidth(Region.USE_COMPUTED_SIZE);
        pane.getStyleClass().add("pane");


        Text text = new Text();
        text.setText(name);
        text.setWrappingWidth(280);
        text.setLayoutX(5);
        text.setLayoutY(20);

        text.setId("saveNameText");

        pane.getChildren().add(text);

        //add on click event to pane elements
        pane.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                updateClickedNode(pane);
            }
        });

        return pane;
    }

    /**
     * updates border color, and current selected node used when displaying selected army
     *
     * @param newClickedNode clicked node
     */
    private void updateClickedNode(Node newClickedNode) {

        if (clickedNode != null)
            this.clickedNode.setStyle("-fx-border-color: gray");

        this.clickedNode = newClickedNode;


        if (clickedNode != null) {
            this.clickedNode.setStyle("-fx-border-color: green");

            updateArmyTable(DataHandler.load(getSaveNameFromCurrentNode()), selectedArmieData, selectedArmyName);

        } else {
            updateArmyTable(null, selectedArmieData, selectedArmyName);
        }

    }

    /**
     * creates elements for every saveName and adds them to vbox
     */
    private void setUpTabel() {
        saveTable.getChildren().clear();

        Arrays.stream(DataHandler.getAllSaveNames()).forEach(e -> {
            saveTable.getChildren().add(
                    createPaneTextNode(e)
            );
        });
    }

    /**
     * casts nodes to correct classes and finds text node
     *
     * @return text from text node
     * @throws RuntimeException if newClickedNode is of wrong foramt
     */
    private String getSaveNameFromCurrentNode() throws RuntimeException {
        try {
            return ((Text) (((Pane) clickedNode).getChildren().get(0))).getText();
        } catch (ClassCastException e) {
            throw new RuntimeException("current clicked node is of wrong format");
        }
    }

    /**
     * updates the data lists witch in turn updates the tables
     *
     * @param army         army to update to
     * @param data         Data list to update
     * @param armyNameNode Army name Text element
     */
    private void updateArmyTable(Army army, ObservableList<UnitTableElement> data, Text armyNameNode) {
        data.clear();
        armyNameNode.setText("Name:");
        if (army == null)
            return;

        data.addAll(army.getUnitTableElements(false));
        armyNameNode.setText(army.getName());
    }

}
