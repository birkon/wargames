package edu.ntnu.idatt2001.wargamesdel1.gui; /**
 * Sample Skeleton for 'gameMenu.fxml' Controller Class
 */

import java.io.IOException;
import java.net.URL;
import java.util.Objects;
import java.util.ResourceBundle;

import edu.ntnu.idatt2001.wargamesdel1.game.GlobalData;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

/**
 * Controller for game menue
 */
public class gameMenuController {

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="ArmiesButton"
    private Button ArmiesButton; // Value injected by FXMLLoader

    /**
     * switch to selectArmies screen
     * @param event
     */
    @FXML
    void switchView(ActionEvent event) {
        try{
            Scene scene = new Scene(FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/selectArmiesScreen.fxml"))), 1071, 626);
            Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
            stage.setScene(scene);
            stage.show();
        }catch (NullPointerException | IOException e){
            e.printStackTrace();
        }

    }

    /**
     * switch to mapGenerationMenue
     * @param event
     */
    @FXML
    void switchToBattleView(ActionEvent event){
        if(GlobalData.getInstance().getCurrentArmy() == null){
            GuiUtil.createAlert("You have to select an army to battle");
            return;
        }
        try{
            Scene scene = new Scene(FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/generateMapMenue.fxml"))), 1071, 626);
            Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
            stage.setScene(scene);
            stage.show();
        }catch (NullPointerException | IOException e){
            e.printStackTrace();
        }
    }

    /**
     * exit game
     */
    @FXML
    void quitMenu(){
        Platform.exit();
        System.exit(0);
    }


}



