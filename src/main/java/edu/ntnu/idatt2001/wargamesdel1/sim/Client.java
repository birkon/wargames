package edu.ntnu.idatt2001.wargamesdel1.sim;

import edu.ntnu.idatt2001.wargamesdel1.army.Army;
import edu.ntnu.idatt2001.wargamesdel1.util.UnitFactory;

/**
 * future client
 * now its used to hardcode a test case
 *
 */
public class Client {
    private final Army army1;
    private final Army army2;

    //defualt hp values and names:
    private final String nameInf = "infantryUnit";
    private final int hpInf = 100;

    private final String nameCav = "cavalryUnit";
    private final int hpCav = 100;

    private final String nameRanged = "rangedUnit";
    private final int hpRanged = 100;

    private final String nameCom = "Comander";
    private final int hpCom = 180;

    /**
     * creates armies on construction of Class
     */
    public Client() {
        this.army1 = new Army("humans");
        this.army2 = new Army("not humans");
    }

    /**
     * loops the different unit types and addes them to army.
     * it also indexes the units (probobly better to do this in the army object)
     * @param army army to add units
     * @param numInf number of infantryUnits
     * @param numCav number of cavUnits
     * @param numRange number of rangedUnits
     * @param numCom number of comanderUnits
     */
    private void createArmy(Army army, int numInf, int numCav, int numRange, int numCom){
        UnitFactory uf = new UnitFactory();
        army.addAll(uf.CreateMultipleUnits(UnitFactory.UnitTypes.INFANTRY,nameInf,hpInf, numInf));

        army.addAll(uf.CreateMultipleUnits(UnitFactory.UnitTypes.CAVALRY,nameCav,hpCav, numCav));

        army.addAll(uf.CreateMultipleUnits(UnitFactory.UnitTypes.RANGED,nameRanged,hpRanged, numRange));

        army.addAll(uf.CreateMultipleUnits(UnitFactory.UnitTypes.COMMANDER,nameCom,hpCom, numCom));


    }


    /**
     * create battle object from army1 and army2 class variabels
     * @return battle object
     */
    public Battle createBattle(){
        createArmy(army1, 500, 100, 200, 1);
        createArmy(army2, 300, 100, 200, 1);
        return new Battle(army1, army2);
    }
}
