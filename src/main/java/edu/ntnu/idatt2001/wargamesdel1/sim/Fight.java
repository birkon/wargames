package edu.ntnu.idatt2001.wargamesdel1.sim;

import edu.ntnu.idatt2001.wargamesdel1.units.Unit;


/**
 * Class for handeling a singel fight
 */
public class Fight {

    //units that will fight
    private final Unit unit1;
    private final Unit unit2;

    //winning and losing units
    private Unit winningUnit = null;
    private Unit losingUnit = null;

    /**
     * creates fight from 2 units
     * @param unit1 first unit
     * @param unit2 second unit
     * @throws IllegalArgumentException if units are null or units are in fight already
     * */
    public Fight(Unit unit1, Unit unit2, Unit.Terrain terrain) throws IllegalArgumentException{
        if (unit1 == null || unit2 == null)
            throw new IllegalArgumentException("Units cant be null");

        if (unit1.isInFight() || unit2.isInFight())
            throw new IllegalArgumentException("Cant create fight with unit already in fight");

        unit1.setInFight(true);
        unit2.setInFight(true);

        unit1.setCurrentTerrain(terrain);
        unit2.setCurrentTerrain(terrain);

        this.unit1 = unit1;
        this.unit2 = unit2;

    }

    /**
     * Method for units to attack eachother until a winner is decided
     * only one attack becouse it will be called in game loop
     * @return true if someone has
     */
    public boolean attackEatchother(){
        if (winningUnit != null)
            return true;


        unit1.attack(unit2);
        if (unit1.getHealth() <= 0){
            winningUnit = unit2;
            losingUnit = unit1;
            return true;
        }
        unit2.attack(unit1);
        if (unit2.getHealth() <= 0){
            winningUnit = unit1;
            losingUnit = unit2;
            return true;
        }

        return false;

    }

    /**
     * returns winning unit
     * @return unit
     */
    public Unit getWinningUnit() {
        return winningUnit;
    }

    /**
     * returns losing unit
     * @return losing unit
     */
    public Unit getLosingUnit() {
        return losingUnit;
    }


}
