package edu.ntnu.idatt2001.wargamesdel1.units;


/**
 * Cavalry unit
 * gets bonus in attack, and small bonus in resistance
 * subclass to Units super class
 * @see Unit
 *
 */
public class CavalryUnit extends Unit{

    // local static default variabels
    // for easy change, but they shoud not be changed when game is running
    // static so they can be used before super class constructer
    private final static int attackBonus = 4;
    private final static int resistBonus = 1;
    private final static int attack = 20;
    private final static int armor = 12;

    private final static double defaultPlainsAttackModifier = 1.5;
    private final static double defaultForrestDefenceModifier = 0.8;


    //changes when getBonusAttack is called
    private boolean charged = false;


    /**
     * sets default modifiers
     */
    @Override
    public void setDefaultTerrainModifiers() {
        putTerrainAttackModifier(Terrain.PLAINS, defaultPlainsAttackModifier);
        putTerrainDefenceModifier(Terrain.FORREST, defaultForrestDefenceModifier);
    }

    /**
     * constructor to assigne values when creation
     * @param name type string
     * @param health type
     * @throws IllegalArgumentException if name is empty or health is less then or equals 0
     */
    public CavalryUnit(String name, int health) throws IllegalArgumentException {
        super(name, health, attack, armor);
    }

    /**
     * constructor to assigne values when creation
     * full constructor in case you want to set custom attack or armor also used in child classes
     *
     * @param name type string
     * @param health type string
     * @param attack type int
     * @param armor type int
     * @throws IllegalArgumentException if name is empty or health is less then or equals 0
     */
    public CavalryUnit(String name, int health, int attack, int armor) throws IllegalArgumentException {
        super(name, health, attack, armor);
    }

    /**
     * overload constructer for deep copying another unit
     * @param unit unit to clone
     */
    public CavalryUnit(Unit unit){
        super(unit);
    }


    /**
     * retunrs the default attack bonus
     * @return attack bonus type int
     */
    @Override
    public int getAttackBonus() {
        int temp_attack = charged ? attackBonus - 2 : attackBonus;
        charged = true;
        return temp_attack;
    }

    /**
     * retunrs the default resist bonus
     * @return resist bonus type int
     */
    @Override
    public int getResistBonus() {
        return resistBonus;
    }
}
