package edu.ntnu.idatt2001.wargamesdel1.units;

import edu.ntnu.idatt2001.wargamesdel1.army.Army;
import edu.ntnu.idatt2001.wargamesdel1.util.UnitFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * abstact class for super too different sub classes
 * exp: cavalery and ranged
 *
 */
public abstract class Unit {

    //pos x and y cords
    public int x;
    public int y;

    //target pos x and y
    public int targetX;
    public int targetY;

    //uniq id
    private int id = -1;

    //true if unit is currently in fight Object
    private boolean isInFight = false;

    //used to have low movement speed even with high frame rate
    private double accumelatedX = 0;
    private double accumelatedY = 0;

    final private String name;
    private int health;
    final private int attack;
    final private int armor;

    //terrain
    final private Map<Terrain, Double> terrainAttackModifiers;
    final private Map<Terrain, Double> terrainDefenceModifiers;


    //current terrain of unit
    private Terrain currentTerrain = Terrain.NO_TERRAIN;



    /**
     * konstructer for assaigning values when super() is used
     * @param name  the name of the unit, type string
     * @param health the health of the unit, type intcc
     * @param attack the attack of the unit, type intcc
     * @param armor the armor of the unit, type intcc
     * @throws IllegalArgumentException  when any value is below 0 and if health is 0 on construction
     * or unit has no name
     */
    protected Unit(String name, int health, int attack, int armor) throws IllegalArgumentException{

        if (health <= 0 || attack < 0 || armor < 0 || name.isEmpty())
            throw new IllegalArgumentException("input values where outside expected range");
        this.name = name;
        this.health = health;
        this.attack = attack;
        this.armor = armor;
        this.terrainAttackModifiers =  new HashMap<>();
        this.terrainDefenceModifiers =  new HashMap<>();


        setDefaultTerrainModifiers();
    }

    /**
     * overload constructer for deep copying another unit
     * @param unit unit to clone
     */
    public Unit(Unit unit){
        this.name = unit.getName();
        this.health = unit.getHealth();
        this.attack = unit.getAttack();
        this.armor = unit.getArmor();
        this.currentTerrain = unit.currentTerrain;
        this.x = unit.x;
        this.y = unit.y;
        this.id = unit.getId();
        this.targetY = unit.targetY;
        this.targetX= unit.targetX;

        //for deep copying the maps
        this.terrainAttackModifiers = new HashMap<> (unit.getTerrainAttackModifiers().entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)));
        this.terrainDefenceModifiers = new HashMap<> (unit.getTerrainDefenceModifiers().entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)));
    }

    /**
     * method to change the health stat of the opponent when its attacked
     * uses the getAttackBonus, and getResistBonus abstact methods
     * deals at minimum 0 damage so they cant regain health
     * @param opponent opponent to inflic damage to, type Unit
     */
    public void attack(Unit opponent){
        opponent.setHealth((int) (opponent.getHealth() - Math.max((
                this.getAttack() + getAttackBonus())* terrainAttackModifier()
                        - (opponent.getArmor() + opponent.getResistBonus()) * opponent.terrainDefenceModifier(), 0)));
    }

    /**
     * sets the health variabel
     * @param health the new health
     */
    public void setHealth(int health){
        this.health = health;
    }

    /**
     * specialised terrain modifer for attack.
     * All terrains not set will return 1
     * NO_TERRAIN will always return 1
     * @return terrain attack modifier defined in subclass
     */
    public double terrainAttackModifier() {
        return Objects.requireNonNullElse(terrainAttackModifiers.get(currentTerrain), 1.0);
    }

    /**
     * specialised terrain modifer for defence.
     * All terrains not set will return 1
     * NO_TERRAIN will always return 1
     * @return terrain defence modifier defined in subclass
     */
    public double terrainDefenceModifier() {
        return Objects.requireNonNullElse(terrainDefenceModifiers.get(currentTerrain), 1.0);
    }

    /**
     * method for setting default terrainModifiers
     * override if unit has default modifiers else leave empty
     */
    public void setDefaultTerrainModifiers() {

    }

    /**
     * @return attack bonus int defined in sub classes
     */
    public abstract int getAttackBonus();

    /**
     * @return resist bonus int defined in sub classes
     */
    public abstract int getResistBonus();

    /**
     * gives the name
     * @return name type string
     */
    public String getName() {
        return name;
    }

    /**
     * gives the health
     * @return health type int
     */
    public int getHealth() {
        return health;
    }

    /**
     * gives the attack
     * @return attack type int
     */
    public int getAttack() {
        return attack;
    }

    /**
     * gives the armor
     * @return armor type int
     */
    public int getArmor() {
        return armor;
    }

    /**
     * sets element in TerrainAttackModifers map by assigning key and value
     * @param terrain Terrain enum
     * @param modifier modifier double
     * @throws IllegalArgumentException if terrain is null or terrain is NO_TERRAIN
     */
    public void putTerrainAttackModifier(Terrain terrain, double modifier) throws IllegalArgumentException{
        if(terrain == null)
            throw new IllegalArgumentException("terrain must be defined");
        else if (terrain == Terrain.NO_TERRAIN)
            throw new IllegalArgumentException("cant assinge modifer to NO_TERRAIN");

        this.terrainAttackModifiers.put(terrain, modifier);
    }

    /**
     * sets element in TerrainDefenceModifers map by assigning key and value
     * @param terrain Terrain enum
     * @param modifier modifier double
     * @throws IllegalArgumentException if terrain is null or terrain is NO_TERRAIN
     */
    public void putTerrainDefenceModifier(Terrain terrain, double modifier) throws IllegalArgumentException{
        if(terrain == null)
            throw new IllegalArgumentException("terrain must be defined");
        else if (terrain == Terrain.NO_TERRAIN)
            throw new IllegalArgumentException("cant assinge modifer to NO_TERRAIN");

        this.terrainDefenceModifiers.put(terrain, modifier);
    }

    /**
     * For copying the terrainModifer map
     * @return TerrainModifiers map
     */
    private Map<Terrain, Double> getTerrainAttackModifiers() {
        return terrainAttackModifiers;
    }

    /**
     * For copying the terrainModifer map
     * @return TerrainModifiers map
     */
    private Map<Terrain, Double> getTerrainDefenceModifiers() {
        return terrainDefenceModifiers;
    }

    /**
     * getter for is in fight variable
     * @return true if isInFight
     */
    public boolean isInFight() {
        return isInFight;
    }

    /**
     * setter for is in fight variable
     */
    public void setInFight(boolean inFight) {
        isInFight = inFight;
    }

    /**
     * checks if two objcts equal
     * used in .contais, and used when removing object from arraylsit in army class
     * @see Army
     * @param o object to check
     * @return if object is equal to check object
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Unit unit)) return false;
        return getHealth() == unit.getHealth() && getAttack() == unit.getAttack() && getArmor() == unit.getArmor() &&
                getName().equals(unit.getName()) && unit.getClass() == getClass() && getId() == unit.getId()
                && x == unit.x && y == unit.y;
    }

    /**
     * overide to ensure equals and hashcode gives same otucome
     * @return hashcode
     */
    @Override
    public int hashCode() {
        return Objects.hash(getName(), getHealth(), getAttack(), getArmor(), getClass(), getId(), x, y);
    }


    /**
     * gives csv format for file storage
     * @return object variables and class separated by ,
     */
    public String getCsvFormat(){
        return this.getClass().getName() +"," + name + "," + health+ "," + attack+ "," + armor;
    }

    /**
     * defuault to string for subclasses
     * @return string value
     */
    @Override
    public String toString() {
        return "[" +
                "name='" + name + '\'' +
                ", health=" + health +
                ", attack=" + attack +
                ", armor=" + armor +
                ']';
    }

    /**
     * terrain enum
     * Terrain will be NO_TERRAIN by default
     * @return the terrain null if no terrain
     */
    public Terrain getCurrentTerrain() {
        return currentTerrain;
    }

    /**
     * sets current terrain
     * also sets currentTerrain to NO_TERRAIN if tarrain givven is null
     * @param currentTerrain terrain to set
     */
    public void setCurrentTerrain(Terrain currentTerrain) {
        this.currentTerrain = Objects.requireNonNullElse(currentTerrain, Terrain.NO_TERRAIN);
    }


    /**
     * converts class name to enum of type
     * @return enum of type
     */
    public UnitFactory.UnitTypes getUnitType(){
        if (InfantryUnit.class.equals(getClass())) {
            return UnitFactory.UnitTypes.INFANTRY;
        }else if (CavalryUnit.class.equals(getClass())) {
            return UnitFactory.UnitTypes.CAVALRY;
        }else if (RangedUnit.class.equals(getClass())) {
            return UnitFactory.UnitTypes.RANGED;
        }else if (ComanderUnit.class.equals(getClass())) {
            return UnitFactory.UnitTypes.COMMANDER;
        }else{
            return null;
        }
    }


    /**
     * moves unit
     * @param x xmove
     * @param y ymove
     */
    public void moveByVektor(double x, double y){

        if (Math.abs(x) <  1)
            accumelatedX += x;
        if (Math.abs(y) < 1)
            accumelatedY += y;

        if (Math.abs(accumelatedX) >= 1){
            x += (int) accumelatedX;
            accumelatedX = 0;
        }

        if (Math.abs(accumelatedY) >= 1){
            y += (int) accumelatedY;
            accumelatedY = 0;
        }

        this.x += (int) x;
        this.y += (int) y;

    }

    /**
     * uniq id of unit
     * @return id
     */
    public int getId() {
        return id;
    }

    /**
     * sets id of unit
     * @param id uniq id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * different possible Terrains
     */
    public enum Terrain{
        PLAINS,
        FORREST,
        HILL,
        NO_TERRAIN
    }
}
