package edu.ntnu.idatt2001.wargamesdel1.util;

import edu.ntnu.idatt2001.wargamesdel1.army.Army;
import edu.ntnu.idatt2001.wargamesdel1.units.*;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;


/**
 * class for handling data and talking to file handler class
 *
 *
 */
public class DataHandler {

    private static final String sourcePath = "src/main/resources/saveFiles/"; //path to saveFiles dir

    /**
     * saves army to file, by generating csv format and calling fileHandler save to file method
     * @param saveName is the name of the save file
     * @param army is the army to be saved
     * @throws IllegalArgumentException if saveName is not allowed
     */
    public static void save(String saveName, Army army) throws IllegalArgumentException{
        if(saveName.equalsIgnoreCase("") || saveName.contains("."))
            throw new IllegalArgumentException("saveName has to be legal file name");

        try {
            FileHandler.saveToFile(
                    army.getName() + "\n"
                            + army.getAllUnits()
                            .stream()
                            .map(Unit::getCsvFormat)
                            .collect(Collectors.joining("\n")),
                    Paths.get(sourcePath + saveName + ".csv"));
        }catch (RuntimeException e){
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * Load army object from file name
     *
     * @param saveName is the name of the save file
     * @return Army from file
     * @throws IllegalArgumentException if saveName is not allowed
     * @throws RuntimeException if data coud not be read from file
     */
    public static Army load(String saveName) throws RuntimeException, IllegalArgumentException{
        if(saveName.equalsIgnoreCase(""))
            throw new IllegalArgumentException("saveName has to be legal file name");

        ArrayList<String> dataArray = FileHandler.readFromFile(Paths.get(sourcePath + saveName + ".csv"));
        if (dataArray == null)
            throw new RuntimeException("No file by that name found");

        ArrayList<Unit> units = new ArrayList<Unit>();

        dataArray.forEach(e->{
            if (dataArray.indexOf(e) != 0) {
                Unit u;
                if ((u = parseUnit(e)) != null)
                    units.add(u);
            }
        });

        return new Army(dataArray.get(0), units);
    }

    /**
     * parses string to unit class
     * @param dataString csv data string
     * @return unit parsed, null if corrupt data
     */
    private static Unit parseUnit(String dataString){
        String[] data = dataString.split(",");
        try {
            Unit unit = new InfantryUnit(data[1],Integer.parseInt(data[2]),Integer.parseInt(data[3]),Integer.parseInt(data[4]));

            Class<?> unitClass = Class.forName(data[0]);

            if (unitClass == CavalryUnit.class)
                return new CavalryUnit(unit);
            else
            if (unitClass== ComanderUnit.class)
                return new ComanderUnit(unit);
            else
            if (unitClass == InfantryUnit.class)
                return new InfantryUnit(unit);
            else
            if (unitClass == RangedUnit.class)
                return new RangedUnit(unit);
        }catch (ClassNotFoundException | IllegalArgumentException | IndexOutOfBoundsException e){
            return null;
        }
        return null;
    }

    /**
     * gets all save names in save folder
     * @return array of save names
     */
    public static String[] getAllSaveNames(){
        String[] allFilesInDir;

        try {
            allFilesInDir = FileHandler.getAllFileNamesInDir(sourcePath);
        }catch (IllegalArgumentException e){
            return new String[0];
        }
        //filters out everything but csv files
        //then removes the .csv
        return Arrays.stream(allFilesInDir).filter(e->e.endsWith(".csv"))
                .map(e->e.replace(".csv", ""))
                .toArray(String[]::new);
    }

    /**
     * deletes given save name.
     * @param saveName to delete
     * @return true if deleted
     */
    public static boolean deleteSave(String saveName){
        return FileHandler.deleteFile(Paths.get(sourcePath + saveName + ".csv"));
    }
}
