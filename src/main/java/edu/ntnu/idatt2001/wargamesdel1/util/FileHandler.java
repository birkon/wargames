package edu.ntnu.idatt2001.wargamesdel1.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * class for handeling reading and writing to file
 *
 *
 */
public class FileHandler {

    /**
     * save to file method
     *
     * @param data csv data string
     * @param path path to file
     */
    public static void saveToFile(String data, Path path) throws RuntimeException{
        File file = new File(String.valueOf(path));
        //to make sure file exists
        try {
            file.createNewFile();
        }catch (IOException e){
            //find the directory path by filtering for .csv keyword
            String dirPath = Arrays.stream(String.valueOf(path).split("\\\\"))
                    .filter(s->!s.contains(".csv"))
                    .collect(Collectors.joining("\\\\"));
            new File(dirPath).mkdir();
            //try to create dir and try again
            try {
                file.createNewFile();
            }catch (IOException er){
                throw new RuntimeException("no file created");
            }
        }

        try {
            Files.writeString(path, data, StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * Method to read from a file
     *
     * @param path path, where to read from
     */
    public static ArrayList<String> readFromFile(Path path) {
        try (BufferedReader lineReader = Files.newBufferedReader(path)) {
            String Dataline;
            ArrayList<String> outputData = new ArrayList<>();
            while ((Dataline = lineReader.readLine()) != null) {
                outputData.add(Dataline);
            }

            lineReader.close();
            return outputData;
        } catch (IOException e) {
            return null;
        }
    }

    /**
     * deletes save file
     * a
     * @param path to save file
     * @return true if file was deleted
     * @throws RuntimeException if deletion times out
     */
    public static boolean deleteFile(Path path){
            File readFile = new File(String.valueOf(path));

            int timeout = 10000;
            int counter = 0;
            readFile.delete();
            while (counter < timeout){
                if (!Files.exists(path))
                    return true;
                counter += 100;
                try{
                    Thread.sleep(100);
                }catch (InterruptedException e){
                    return false;
                }
            }
            throw new RuntimeException("Deletion timeout");
    }

    /**
     * returns all file names in dir.
     * used when listing save files
     * @param path path to dir
     * @return list of all file names empty if none exsists or path is not a dir
     * @throws IllegalArgumentException if directory dose not exist
     * @throws IllegalArgumentException if path is not to a directory
     */
    public static String[] getAllFileNamesInDir(String path) throws IllegalArgumentException{
        if(Files.notExists(Path.of(path)))
            throw new IllegalArgumentException("Directory dose not exist");

        File directoryPath = new File(path);

        String[] listOfFileNames = directoryPath.list();

        if (listOfFileNames == null)
            throw new IllegalArgumentException("Path not to directory");

        return listOfFileNames;
    }
}
