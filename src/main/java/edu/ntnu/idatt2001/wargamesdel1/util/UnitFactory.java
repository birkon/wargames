package edu.ntnu.idatt2001.wargamesdel1.util;

import edu.ntnu.idatt2001.wargamesdel1.units.*;

import java.util.Arrays;
import java.util.List;

/**
 * factory class for creation of units
 */
public class UnitFactory {

    /**
     * enum for different unit types.
     */
    public enum UnitTypes{
        CAVALRY,
        COMMANDER,
        INFANTRY,
        RANGED
    }

    /**
     * Creates units from type name and health
     * @param unitType Enum of unit type
     * @param name name of unit
     * @param health health of unit
     * @return Unit Created unit
     * @throws IllegalArgumentException if unit construction throws exception
     * @throws IllegalArgumentException if unit type where not found
     * @throws IllegalArgumentException if unit type is null
     */
    public Unit CreateUnit(UnitTypes unitType, String name, int health) throws IllegalArgumentException{
        if(unitType == null)
            throw new IllegalArgumentException("unit type is null");

        switch (unitType) {
            case CAVALRY -> {
                return new CavalryUnit(name, health);
            }
            case COMMANDER -> {
                return new ComanderUnit(name, health);
            }

            case INFANTRY -> {
                return new InfantryUnit(name, health);
            }
            case RANGED -> {
                return new RangedUnit(name, health);
            }
            default -> throw new IllegalArgumentException("unit type not found");
        }

    }

    /**
     * Creates a list of units by using CreateUnit method
     * @param unitType Enum of unit type
     * @param name name of unit
     * @param health health of unit
     * @return Unit Created unit
     * @throws IllegalArgumentException if unit creation receives illegal Arguments
     */
    public List<Unit> CreateMultipleUnits(UnitTypes unitType, String name, int health, int numberOfUnits)
            throws IllegalArgumentException{

        //creates empty list of wanted size then maps to create unit method
        return Arrays.stream(new Unit[numberOfUnits])
                .map(e -> CreateUnit(unitType, name, health))
                .toList();
    }
}
