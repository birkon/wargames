module edu.ntnu.idatt2001.wargamesdel1 {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires org.kordamp.bootstrapfx.core;

    opens edu.ntnu.idatt2001.wargamesdel1.gui to javafx.fxml;
    exports edu.ntnu.idatt2001.wargamesdel1.gui;

    opens edu.ntnu.idatt2001.wargamesdel1.army to javafx.fxml;
    exports edu.ntnu.idatt2001.wargamesdel1.army;

    exports edu.ntnu.idatt2001.wargamesdel1.util;

    exports edu.ntnu.idatt2001.wargamesdel1.units;

}