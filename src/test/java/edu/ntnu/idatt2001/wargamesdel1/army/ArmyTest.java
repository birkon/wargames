package edu.ntnu.idatt2001.wargamesdel1.army;

import edu.ntnu.idatt2001.wargamesdel1.units.InfantryUnit;
import edu.ntnu.idatt2001.wargamesdel1.units.RangedUnit;
import edu.ntnu.idatt2001.wargamesdel1.units.Unit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class ArmyTest {
    Army testArmy;
    ArrayList<Unit> testUnits;
    @BeforeEach
    void setUp() {
        testArmy = new Army("army1");
        testUnits = new ArrayList<Unit>();

        int len = (int)(Math.random()*200 + 200);
        for (int i = 0; i < len; i++) {
            testUnits.add(new InfantryUnit("testinf", 100));
        }

        testArmy.addAll(testUnits);
    }

    @Test
    void addUnit() {
        ArrayList<Unit> testUnits2 = new ArrayList<Unit>();
        int len = (int)(Math.random()*200 + 200);
        for (int i = 0; i < len; i++) {
            testUnits2.add(new InfantryUnit("testinf", 100));
        }

        testArmy.addAll(testUnits2);
        assertEquals(testUnits2.size(), len);
    }

    @Test
    void remove() {
        int preRemove = testArmy.getAllUnits().size();

        Unit randomUnit = testArmy.getRandomUnit();
        testArmy.remove(testArmy.getRandomUnit());

        assertNotEquals(preRemove, testArmy.getAllUnits().size());
    }

    @Test
    void getRandomUnit() {
        Unit ranUnit = testArmy.getRandomUnit();
        ranUnit.setHealth(99);
        ranUnit = testArmy.getRandomUnit();
        assertNotEquals(ranUnit.getHealth(), 99);
    }

    @Test
    void getInfatryUnits(){
        int len = (int)(Math.random()*100 + 100);
        for (int i = 0; i < len; i++) {
            testUnits.add(new RangedUnit("RangedUnit", 100));
        }
        ArrayList<Unit> infantryUnits = new ArrayList<>(testArmy.getInfatryUnits());
        final boolean[] isTrue = {true};
        infantryUnits.forEach(e-> {if(e.getClass() != InfantryUnit.class) isTrue[0] = false;});
        assertNotEquals(infantryUnits.size(), 0);
        System.out.println(infantryUnits.size());
        assertTrue(isTrue[0]);
    }

    @Test
    void getRangedUnit(){
        int len = (int)(Math.random()*100 + 100);
        testUnits = new ArrayList<Unit>();
        for (int i = 0; i < len; i++) {
            testUnits.add(new RangedUnit("RangedUnit", 100));
        }
        testArmy.addAll(testUnits);
        ArrayList<Unit> rangedUnits = new ArrayList<>(testArmy.getRangedUnit());
        final boolean[] isTrue = {true};
        rangedUnits.forEach(e-> {if(e.getClass() != RangedUnit.class) isTrue[0] = false;});
        assertNotEquals(rangedUnits.size(), 0);
        System.out.println(rangedUnits.size());
        assertTrue(isTrue[0]);
    }

}