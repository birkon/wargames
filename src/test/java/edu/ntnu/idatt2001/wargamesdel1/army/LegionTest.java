package edu.ntnu.idatt2001.wargamesdel1.army;

import edu.ntnu.idatt2001.wargamesdel1.game.GlobalData;
import edu.ntnu.idatt2001.wargamesdel1.game.MapGenerator;
import edu.ntnu.idatt2001.wargamesdel1.units.Unit;
import edu.ntnu.idatt2001.wargamesdel1.util.UnitFactory;
import javafx.scene.image.Image;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.*;


class LegionTest {

    Army testArmy;
    Legion testLegain;
    Legion testLegain2;


    @BeforeEach
    void setUp() {
        UnitFactory uf = new UnitFactory();
        testArmy = new Army("TEST");
        testArmy.addAll(uf.CreateMultipleUnits(UnitFactory.UnitTypes.INFANTRY, "test", 123, 50));
        testLegain = new Legion(testArmy.getInfatryUnits(), 250, 250, 5);

        testArmy = new Army("TEST2");

        testArmy.addAll(uf.CreateMultipleUnits(UnitFactory.UnitTypes.INFANTRY, "test", 123, 50)
                .stream().peek(unit -> unit.x = 50).toList());

        testLegain2 = new Legion(testArmy.getInfatryUnits(), 500, 250, 5);


    }


    @Test
    void moveByVektor() {
        int preX = testLegain.getUnits().get(0).x;
        testLegain.moveByVektor(10, 0);
        assertEquals(testLegain.getUnits().get(0).x,preX +10);
        testLegain.moveByVektor(-0.5, 0);
        testLegain.moveByVektor(-0.5, 0);
        assertEquals(testLegain.getUnits().get(0).x,preX +10 -1);
        testLegain.setCanMove(false);
        testLegain.moveByVektor(10, 0);
        assertEquals(testLegain.getUnits().get(0).x,preX +10 -1);

    }

    @Test
    void getUnits() {
        assert testLegain.getUnits().size() > 0;
        Unit unit1 = testLegain.getUnits().get(0);
        unit1.x += 10;
        assertNotEquals(unit1.x, testLegain.getUnits().get(0).x);
    }


    @Test
    void activateUnits() {
        testLegain.setCanMove(false);
        testLegain2.setCanMove(false);


        testLegain.activateUnits(testLegain2);
        testLegain2.activateUnits(testLegain);

        assertNotEquals (0 ,testLegain.getNumberOfActivatedUnits());
        assertNotEquals (0 ,testLegain2.getNumberOfActivatedUnits());
    }

    @Test
    void setTargetAndMove(){

        UnitFactory uf = new UnitFactory();
        testArmy = new Army("TEST");
        testArmy.addAll(uf.CreateMultipleUnits(UnitFactory.UnitTypes.INFANTRY, "test", 123, 1).stream().peek(unit -> {
            unit.x = 10;
            unit.y = 10;
        }).toList());
        testLegain = new Legion(testArmy.getInfatryUnits(), 0, 250, 5);

        testArmy = new Army("TEST2");

        testArmy.addAll(uf.CreateMultipleUnits(UnitFactory.UnitTypes.INFANTRY, "test", 123, 1).stream().peek(unit -> {
            unit.x = 80;
            unit.y = 10;
        }).toList());
        MapGenerator mp = new MapGenerator(900, 500, 0.5,  0.7,
                91323128L,
                (int)5, 1.5);
        Image img = mp.getImageOfMap();
        GlobalData.getInstance().setMapImg(img);

        testLegain2 = new Legion(testArmy.getInfatryUnits(), 259, 250, 5);

        testLegain.setCanMove(false);
        testLegain2.setCanMove(false);


        //both need to be activated, then call the method  again to set targets
        assert !testLegain.activateUnits(testLegain2);
        assert !testLegain2.activateUnits(testLegain);
        assert !testLegain.activateUnits(testLegain2);
        assert !testLegain2.activateUnits(testLegain);

        testLegain.moveActivatedUnits(10, 1);

        assertNotEquals(testLegain.getActiavtedUnits().get(0).targetX, 0);
        assertNotEquals(testLegain.getActiavtedUnits().get(0).x, 10);

        assertNotEquals (0 ,testLegain.getNumberOfActivatedUnits());
        assertNotEquals (0 ,testLegain2.getNumberOfActivatedUnits());
    }



    @Test
    void getNumberOfActivatedUnits() {
        testLegain.activateUnits(testLegain2);
        testLegain2.activateUnits(testLegain);
        assertNotEquals (0 ,testLegain.getNumberOfActivatedUnits());
    }

    @Test
    void getNumberOfTotalUnits() {
        testLegain.activateUnits(testLegain2);
        assertNotEquals (testLegain.getUnits().size(),testLegain.getNumberOfTotalUnits());
    }

}