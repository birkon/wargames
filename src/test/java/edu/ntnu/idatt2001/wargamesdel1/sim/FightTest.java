package edu.ntnu.idatt2001.wargamesdel1.sim;

import edu.ntnu.idatt2001.wargamesdel1.units.InfantryUnit;
import edu.ntnu.idatt2001.wargamesdel1.units.Unit;
import edu.ntnu.idatt2001.wargamesdel1.util.UnitFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


class FightTest {
    Fight testFight;
    Unit u1;
    Unit u2;

    @BeforeEach
    void setUp() {
        UnitFactory uf = new UnitFactory();
        u1 = uf.CreateUnit(UnitFactory.UnitTypes.INFANTRY, "test1", 200);
        u2 = uf.CreateUnit(UnitFactory.UnitTypes.INFANTRY, "test2", 200);
        testFight = new Fight(new InfantryUnit(u1), new InfantryUnit(u2), Unit.Terrain.FORREST);
    }

    @Test
    void attackEatchOtherAndTerrain() {
        while (u1.getHealth() > 0 && u2.getHealth() > 0){
            u1.attack(u2);
            u2.attack(u1);
        }

        while (!testFight.attackEatchother()){
            //loop the attack until one wins
        }

        assertNotEquals(testFight.getWinningUnit().getHealth(), u1.getHealth());
        assertNotEquals(testFight.getWinningUnit().getHealth(), u2.getHealth());

    }

    @Test
    void createFight() {
        assertThrows(IllegalArgumentException.class, ()->new Fight(u1, null, Unit.Terrain.FORREST));
        assertThrows(IllegalArgumentException.class, ()->new Fight(u1, u2, null));
    }

}