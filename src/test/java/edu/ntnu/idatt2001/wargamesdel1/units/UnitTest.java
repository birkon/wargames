package edu.ntnu.idatt2001.wargamesdel1.units;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UnitTest {

    InfantryUnit unit1;
    InfantryUnit unit2;

    @BeforeEach
    void setUp() {
        unit1 = new InfantryUnit("test", 100);
        unit2 = new InfantryUnit("test2", 100);
    }

    @Test
    void attack(){
        unit1.attack(unit2);
        assertTrue(unit1.getHealth() > unit2.getHealth());
        assertEquals(unit2.getHealth(), 100 - Math.max(unit1.getAttack() + unit1.getAttackBonus()
                - (unit2.getArmor() + unit2.getResistBonus()), 0));
    }

    @Test
    void getAndSetTerrain(){
        assertNotNull(unit1.getCurrentTerrain());
        unit1.setCurrentTerrain(Unit.Terrain.PLAINS);
        assertEquals(unit1.getCurrentTerrain(), Unit.Terrain.PLAINS);

        unit1.setCurrentTerrain(null);

        assertEquals(Unit.Terrain.NO_TERRAIN, unit1.getCurrentTerrain());

        unit1.setCurrentTerrain(Unit.Terrain.NO_TERRAIN);

        assertEquals(Unit.Terrain.NO_TERRAIN, unit1.getCurrentTerrain());

        unit1.setCurrentTerrain(Unit.Terrain.PLAINS);

        assertEquals(Unit.Terrain.PLAINS, unit1.getCurrentTerrain());
    }

    @Test
    void checkTerrainModifiers(){
        unit1.setCurrentTerrain(Unit.Terrain.HILL);
        assertEquals(1, unit1.terrainAttackModifier());

        unit1.setCurrentTerrain(Unit.Terrain.FORREST);
        assertEquals(1.2, unit1.terrainAttackModifier());

        assertEquals(1.2, unit1.terrainDefenceModifier());

        assertThrows(IllegalArgumentException.class,
                ()-> unit1.putTerrainAttackModifier(Unit.Terrain.NO_TERRAIN, 69));

        unit1.setCurrentTerrain(Unit.Terrain.PLAINS);
        unit1.putTerrainAttackModifier(Unit.Terrain.PLAINS, 69);
        assertEquals(69, unit1.terrainAttackModifier());

        unit1.putTerrainAttackModifier(Unit.Terrain.PLAINS, 20);
        assertEquals(20, unit1.terrainAttackModifier());

        unit1.setCurrentTerrain(Unit.Terrain.NO_TERRAIN);
        assertEquals(1, unit1.terrainAttackModifier());
    }
}