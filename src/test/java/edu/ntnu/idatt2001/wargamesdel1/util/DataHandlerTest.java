package edu.ntnu.idatt2001.wargamesdel1.util;

import edu.ntnu.idatt2001.wargamesdel1.army.Army;
import edu.ntnu.idatt2001.wargamesdel1.units.CavalryUnit;
import edu.ntnu.idatt2001.wargamesdel1.units.ComanderUnit;
import edu.ntnu.idatt2001.wargamesdel1.units.InfantryUnit;
import edu.ntnu.idatt2001.wargamesdel1.units.RangedUnit;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;

import java.io.File;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.*;

class DataHandlerTest {
    private Army army;

    @BeforeEach
    void setup(){
        //save name is set so its very unlikly to overide saveNames during development
        army = new Army("testArmydajaw8de209318jsduja928");

        for (int i = 0; i < 100*Math.random() + 10; i++) {
            army.addUnit(new InfantryUnit("inf", 100));
        }
        for (int i = 0; i < 50 * Math.random() + 10; i++) {
            army.addUnit(new CavalryUnit("cav",  150));
        }
        for (int i = 0; i < 40 * Math.random() + 10 ; i++) {
            army.addUnit(new RangedUnit("ranged", 50));
        }
        for (int i = 0; i < 2 * Math.random() + 1; i++) {
            army.addUnit(new ComanderUnit("commander", 400));
        }
    }

    void createTestSavesForVisualTesting(){
        DataHandler.save("army111111111111111111111111111111111111111111111111111111111111111111111111111111", army);
        setup();
        DataHandler.save("army211111111111111111111111111111111111111111111111111111111111111111111111111111", army);
        setup();
        DataHandler.save("army311111111111111111111111111111111111111111111111111111111111111111111111111111", army);
        setup();
        DataHandler.save("army411111111111111111111111111111111111111111111111111111111111111111111111111111", army);
        setup();
        DataHandler.save("army511111111111111111111111111111111111111111111111111111111111111111111111111111", army);
        setup();
        DataHandler.save("army1611111111111111111111111111111111111111111111111111111111111111111111111111111", army);
        setup();
        DataHandler.save("army2711111111111111111111111111111111111111111111111111111111111111111111111111111", army);
        setup();
        DataHandler.save("army3811111111111111111111111111111111111111111111111111111111111111111111111111111", army);
        setup();
        DataHandler.save("army4911111111111111111111111111111111111111111111111111111111111111111111111111111", army);
        setup();
        DataHandler.save("army51111111111111111111111111111111111111111111111111111111111111111111111111111111", army);
    }

    @Test
    void save() {
        assert army != null;
        DataHandler.save(army.getName(), army);
        assertThrows(IllegalArgumentException.class, ()-> DataHandler.save("", army));
        assertThrows(IllegalArgumentException.class, ()->DataHandler.save("dlpaojwi/ad.txt", army));
        File file = new File("src/main/resources/saveFiles/testArmydajaw8de209318jsduja928.csv");
        file.deleteOnExit();
        assertTrue(file.exists());
    }

    @Test
    void load() {
        assert army != null;
        DataHandler.save(army.getName(), army);
        assertThrows(RuntimeException.class, ()-> DataHandler.load("adwar123 123asdad asdwq"));
        assertThrows(IllegalArgumentException.class, ()-> DataHandler.load(""));

        Army army2 = DataHandler.load(army.getName());
        assertNotNull(army);

        assertEquals(army2.getName(), "testArmydajaw8de209318jsduja928");

        try {
            Thread.sleep(1000);
        }catch (InterruptedException ignored){

        }
        assertTrue(FileHandler.deleteFile(Paths.get("src/main/resources/saveFiles/testArmydajaw8de209318jsduja928.csv")));

    }

    @Test
    void getSaveNames(){
        String[] saveNames = DataHandler.getAllSaveNames();
        assertNotNull(saveNames);

        DataHandler.save(army.getName(), army);

        saveNames = DataHandler.getAllSaveNames();

        assertNotEquals(0, saveNames.length);

        File file = new File("src/main/resources/saveFiles/testArmydajaw8de209318jsduja928.csv");
        file.deleteOnExit();

    }


}