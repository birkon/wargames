package edu.ntnu.idatt2001.wargamesdel1.util;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FileHandlerTest {

    @BeforeEach
    void setUp() {
    }

    @Test
    void getAllFileNamesInDir() {
        String[] testArray = FileHandler.getAllFileNamesInDir("src/main/resources/saveFiles/");
        assertNotNull(testArray);

        assertThrows(IllegalArgumentException.class, ()-> FileHandler.getAllFileNamesInDir("src/main/resources/saveFiles/test.csv"));

        assertThrows(IllegalArgumentException.class, ()-> FileHandler.getAllFileNamesInDir("src/main/resources/NOTEXISTINGDIR/"));


    }
}