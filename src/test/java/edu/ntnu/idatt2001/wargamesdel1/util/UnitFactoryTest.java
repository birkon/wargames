package edu.ntnu.idatt2001.wargamesdel1.util;

import edu.ntnu.idatt2001.wargamesdel1.units.CavalryUnit;
import edu.ntnu.idatt2001.wargamesdel1.units.InfantryUnit;
import edu.ntnu.idatt2001.wargamesdel1.units.Unit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class UnitFactoryTest {
    UnitFactory fac;

    @BeforeEach
    void setUp() {
        fac = new UnitFactory();
    }

    @Test
    void createUnit() {
        //default test
        Unit testUnit = fac.CreateUnit(UnitFactory.UnitTypes.CAVALRY, "test1", 100);
        assertNotNull(testUnit);
        assertEquals(100, testUnit.getHealth());
        assertEquals("test1", testUnit.getName());
        assertEquals(testUnit.getClass(), CavalryUnit.class);

        assertEquals(fac.CreateUnit(UnitFactory.UnitTypes.INFANTRY, "test1", 100).getClass(),
                InfantryUnit.class);

        //check if method throws correctly
        assertThrows(IllegalArgumentException.class,
                ()->fac.CreateUnit(UnitFactory.UnitTypes.CAVALRY, "test1", -100));
        assertThrows(IllegalArgumentException.class,
                ()->fac.CreateUnit(UnitFactory.UnitTypes.CAVALRY, "", 100));
        assertThrows(IllegalArgumentException.class,
                ()->fac.CreateUnit(null, "test1", 100));

    }

    @Test
    void createMultipleUnits() {
        List<Unit> testUnitList =
                fac.CreateMultipleUnits(UnitFactory.UnitTypes.CAVALRY, "test1", 100, 100);

        assertEquals(100, testUnitList.size());
        assertEquals(testUnitList.get(0).getClass(), CavalryUnit.class);

        assertEquals(25,
                fac.CreateMultipleUnits(UnitFactory.UnitTypes.CAVALRY, "test1", 100, 25).size());
    }
}